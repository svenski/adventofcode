import io
import pytest
import string

def main():

    input_file = 'inputs/day_5.txt'
    with open(input_file, 'r') as in_file: 
        poly = in_file.read()

    part_1(poly)
    part_2(poly)

def part_1(poly):

    # hacky anchoring
    poly = poly + '@'

    current_position = 0
    current_poly = poly

    while current_poly[current_position + 1] != '@':
        if reacts(current_poly[current_position], current_poly[current_position + 1]):
            current_poly = make_reaction(current_poly, current_position)
            current_position = max(0, current_position -1)
        else:
            current_position += 1

    return len(current_poly[:-1])

def part_2(poly):

    smallest = len(poly)
    for ll in string.ascii_lowercase:
        this_poly = poly.replace(ll, "").replace(ll.upper(), "")
        poly_len = part_1(this_poly)
        if poly_len < smallest:
            smallest = poly_len
            
    return smallest

def make_reaction(poly, pos):
    return poly[:pos] + poly[pos+2:]

def reacts(a,b):
    if a.lower() == b.lower():
        if a.swapcase() == b:
            return True

    return False


class TestPart1(object):

    def test_example(self):
        input_string = """dabAcCaCBAcCcaDA"""
        out = part_1(input_string)

        assert out == 10

    def test_reacts(self):

        assert reacts('a', 'A') == True
        assert reacts('B', 'A') == False
        assert reacts('B', 'b') == True
        assert reacts('b', 'b') == False

class TestPart2(object):

    def test_example(self):
        input_string = """dabAcCaCBAcCcaDA"""
        out = part_2(input_string)

        assert out == 4


