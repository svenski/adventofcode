import pandas as pd
import numpy as np

from dataclasses import dataclass
import pytest

import re
import matplotlib.pyplot as pp



def main():
    with open('inputs/day_10.txt', 'r') as ii:
        input_string = ii.read()

    part_1(input_string)
    part_2(input_string)
    

def part_1(input_string):

    lights = parse_input(input_string)

    steps = 20000
    metrics = []

    best_y_so_far = 10000000
    best_light_coord = None

    for ss in range(steps):

        y_size = total_length(lights.pos_y)
        metrics.append(pd.DataFrame({'step': ss, 
            'x_size' : total_length(lights.pos_x), 
            'y_size' : y_size,
            'max_aligned_x' : max_aligned(lights.pos_x),
            'max_aligned_y' : max_aligned(lights.pos_y)}, index = [0]))

        if y_size < best_y_so_far:
            best_light_coord = lights.copy()
            best_y_so_far = y_size

        lights = advance(lights)


    metrics = pd.concat(metrics, ignore_index=True)
    metrics.sort_values('x_size')
    metrics.sort_values('y_size')

    display(best_light_coord)
    best_y_so_far

    

def total_length(x):
    return x.max() - x.min()

def max_aligned(x):
    return x.value_counts().values[0]



def display(lights):

    min_x = lights.pos_x.min()
    max_x = lights.pos_x.max()
    size_x = max_x - min_x
    min_y = lights.pos_y.min()
    max_y = lights.pos_y.max()
    size_y = max_y - min_y

    # sky = np.chararray((size_x+1, size_y+1))
    # sky[:] = '.'

    sky = np.zeros((size_x+1, size_y+1))

    # light_sky_coords = np.array(list(zip(lights.pos_x - min_x, lights.pos_y - min_y)))

    x_sky = lights.pos_x - min_x
    y_sky = lights.pos_y - min_y

    sky[x_sky, y_sky] = 1

    print(pd.DataFrame(sky).transpose().to_string())
    pp.matshow(np.transpose(sky))

    
def advance(lights):
    lights['pos_x'] = lights['pos_x'] + lights['vel_x']
    lights['pos_y'] = lights['pos_y'] + lights['vel_y']
    return lights


def parse_input(input_string):
    inputs = input_string.splitlines()

    pattern = "position=<\s?(-?\d+),\s+(-?\d+)> velocity=<\s?(-?\d+),\s+(-?\d+)>"

    lights = []

    for light_string in inputs:
        mm = re.match(pattern, light_string)
        # lights.append(Light(Point(int(mm.group(1)), int(mm.group(2))), Point( int(mm.group(3)), int(mm.group(4)))))
        lights.append(pd.DataFrame([[int(mm.group(1)), int(mm.group(2)),  int(mm.group(3)), int(mm.group(4))]], columns = ['pos_x', 'pos_y', 'vel_x', 'vel_y']))


    return pd.concat(lights, ignore_index=True)


def find_max(lights):

    max_x = 0
    max_y = 0

    for ll in lights:
        pp = ll.pos
        if(pp.x > max_x): max_x = pp.x
        if(pp.y > max_y): max_y = pp.y

    return max_x, max_y


@dataclass
class Point:
    x: int
    y: int

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

@dataclass
class Light:
    pos: Point
    vel: Point

    def advance(self):
        self.pos = self.pos + self.vel


class TestPart1(object):

    def test_example(self):
        input_string = """position=< 9,  1> velocity=< 0,  2>
position=< 7,  0> velocity=<-1,  0>
position=< 3, -2> velocity=<-1,  1>
position=< 6, 10> velocity=<-2, -1>
position=< 2, -4> velocity=< 2,  2>
position=<-6, 10> velocity=< 2, -2>
position=< 1,  8> velocity=< 1, -1>
position=< 1,  7> velocity=< 1,  0>
position=<-3, 11> velocity=< 1, -2>
position=< 7,  6> velocity=<-1, -1>
position=<-2,  3> velocity=< 1,  0>
position=<-4,  3> velocity=< 2,  0>
position=<10, -3> velocity=<-1,  1>
position=< 5, 11> velocity=< 1, -2>
position=< 4,  7> velocity=< 0, -1>
position=< 8, -2> velocity=< 0,  1>
position=<15,  0> velocity=<-2,  0>
position=< 1,  6> velocity=< 1,  0>
position=< 8,  9> velocity=< 0, -1>
position=< 3,  3> velocity=<-1,  1>
position=< 0,  5> velocity=< 0, -1>
position=<-2,  2> velocity=< 2,  0>
position=< 5, -2> velocity=< 1,  2>
position=< 1,  4> velocity=< 2,  1>
position=<-2,  7> velocity=< 2, -2>
position=< 3,  6> velocity=<-1, -1>
position=< 5,  0> velocity=< 1,  0>
position=<-6,  0> velocity=< 2,  0>
position=< 5,  9> velocity=< 1, -2>
position=<14,  7> velocity=<-2,  0>
position=<-3,  6> velocity=< 2, -1>
"""
        out = part_1(input_string)


