import numpy as np
import pandas as pd

import pytest

import re
import io

import itertools

from collections import defaultdict

def main():
    input_file = 'inputs/day_7.txt'
    with open(input_file, 'r') as infile:
        input_string = infile.read()

    part_1(input_string)
    part_2(input_string)


def part_1(input_string):

    unique_nodes, deps = get_nodes(input_string)
    remaining_nodes = unique_nodes.copy()
    remaining_deps = deps.copy()

    order = []

    while len(remaining_nodes) > 0:
        nodes_with_no_deps = remaining_nodes  - set(remaining_deps.step.values)
        next_node = sorted(nodes_with_no_deps)[0]
        order.append(next_node)
        remaining_deps = remaining_deps[remaining_deps.dep_on != next_node]
        remaining_nodes.remove(next_node)


    return "".join(order)
        

def get_nodes(input_string):
    nodes = []

    pattern = r'Step (\w) must be finished before step (\w) can begin.'
    for row in input_string.splitlines():
        mm = re.match(pattern, row)
        nodes.append([mm.group(1), mm.group(2)])

    unique_nodes = set(itertools.chain(*nodes))
    deps = pd.DataFrame(nodes, columns = ['dep_on', 'step'])
    
    return unique_nodes, deps

def part_2(input_string, number_of_workers=5, base_cost=60):
  
    unique_nodes, deps = get_nodes(input_string)
    remaining_nodes = unique_nodes.copy()
    remaining_deps = deps.copy()

    time = 0
    working_nodes = defaultdict(list)
    order = []
    currently_remaining_workers = number_of_workers

    while (len(remaining_nodes) > 0) or (len(working_nodes) > 0):
        nodes_processed_now = sorted(working_nodes[time])
        for nn in nodes_processed_now:
            order.append(nn)
            remaining_deps = remaining_deps[remaining_deps.dep_on != nn]
            currently_remaining_workers += 1
            del working_nodes[time]

        while currently_remaining_workers > 0:
            nodes_with_no_deps = remaining_nodes  - set(remaining_deps.step.values)
            if len(nodes_with_no_deps) > 0:
                next_node = sorted(nodes_with_no_deps)[0]

                node_cost = time_for_node(next_node, base_cost)
                working_nodes[time + node_cost].append(next_node)
                remaining_nodes.remove(next_node)
                currently_remaining_workers -= 1
            else:
                break

        if len(working_nodes[time]) == 0:  
            del working_nodes[time]

        time +=1

    return time - 1


def time_for_node(next_node, base_cost):
    return ord(next_node) - 64 + base_cost




class TestPart2(object):

    def test_example(self):
        input_string = """Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.
        """
        out = part_2(input_string, number_of_workers=2, base_cost=0)

        assert out == 15

class TestPart1(object):

    def test_example(self):
        input_string = """Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.
        """
        out = part_1(input_string)

        assert out == 'CABDFE'

