import io
import pytest
import string

from dataclasses import dataclass
from dataclasses import field

import numpy as np

def main():
    with open('inputs/day_8.txt', 'r') as ii:
        input_string = ii.read()

    part_1(input_string)
    part_2(input_string)


def part_1(input_string):
    inputs = parse_string_to_list(input_string)
    tree,_ = create_tree_for(inputs)

    return sum_of_meta_for(tree)

def part_2(input_string):
    inputs = parse_string_to_list(input_string)
    tree,_ = create_tree_for_part2(inputs)

    return tree.metadata[0]

def sum_of_meta_for(tree):
    return sum(tree.metadata) + sum(map(sum_of_meta_for, tree.nodes))
    

import ipdb

def create_tree_for_part2(inputs):

    number_of_children = inputs[0]
    number_of_metadata = inputs[1]
    nodes = []

    start_char_for_next = 2
    
    for cc in range(number_of_children):
        next_top_node, characters_used  = create_tree_for_part2(inputs[start_char_for_next:])
        nodes.append(next_top_node)
        start_char_for_next += characters_used

    if number_of_children == 0:
        return Node(nodes=nodes, metadata=[sum(inputs[start_char_for_next:(start_char_for_next+number_of_metadata)])]), start_char_for_next+number_of_metadata
    else:

        indexes_for_meta = inputs[start_char_for_next:(start_char_for_next+number_of_metadata)]
        total = 0
        for ii in indexes_for_meta:
            ii = ii - 1
            if ii < 0: continue
            if ii > (len(nodes) -1):continue
            total += sum(nodes[ii].metadata)

        return Node(nodes=nodes, metadata=[total]), start_char_for_next+number_of_metadata
            
def create_tree_for(inputs):

    number_of_children = inputs[0]
    number_of_metadata = inputs[1]
    nodes = []

    start_char_for_next = 2
    for cc in range(number_of_children):
        next_top_node, characters_used  = create_tree_for(inputs[start_char_for_next:])
        nodes.append(next_top_node)
        start_char_for_next += characters_used

    return Node(nodes=nodes, metadata=inputs[start_char_for_next:(start_char_for_next+number_of_metadata)]), start_char_for_next+number_of_metadata



def parse_string_to_list(input_string):
    inputs = input_string.split(' ')
    return list(map(int, inputs))

@dataclass
class Node:
    nodes: list#List[Node] = field(default_factory=list)
    metadata: list#List[int] = field(default_factory=list)



class TestPart1(object):

    def test_example(self):
        input_string = """2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2"""
        out = part_1(input_string)

        assert out == 138


    def test_one_tree(self):
        inputs = parse_string_to_list("0 2 11 13")
        trees,char = create_tree_for(inputs)
        assert trees == Node(nodes = [], metadata = [11,13])

    def test_another_tree(self):
        inputs = parse_string_to_list("0 3 1 2 3")
        trees,char = create_tree_for(inputs)
        assert trees == Node(nodes = [], metadata = [1,2,3])

    def test_an_empty_root_node_with_child(self):
        inputs = parse_string_to_list( "1 0 0 2 3 4")
        trees,_ = create_tree_for(inputs)
        assert trees == Node(nodes = [Node(nodes=[], metadata=[3,4])], metadata = [])
        
    def test_an_non_empty_root_node_with_child(self):
        inputs = parse_string_to_list( "1 1 0 1 3 4")
        trees,_ = create_tree_for(inputs)
        assert trees == Node(nodes = [Node(nodes=[], metadata=[3])], metadata = [4])



