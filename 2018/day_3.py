import numpy as np
import pandas as pd

import io
import re

import pytest

FABRIC_EDGE = 1000

def main():

    input_file = 'inputs/day_3.tsv'
    part_1(input_file)
    part_2(input_file)


def part_1(input_file):
    raw_claims = read_from(input_file)
    claims = parse(raw_claims)
    fabric = fabric_for(claims)

    return len(fabric[fabric >= 2])


def part_2(input_file):
    raw_claims = read_from(input_file)
    claims = parse(raw_claims)
    fabric = fabric_for(claims)

    for index, claim in claims.iterrows():
        if (fabric[slice_for(claim)] == 1).all():
            break

    return claim.id


def parse(claims):

    parsed_claims = []

    pattern = r"(\d+) @ (\d+),(\d+): (\d+)x(\d+)$"

    for claim in claims:
        match = re.search(pattern, claim)
        parsed_claims.append(pd.DataFrame({
            'id': int(match.group(1)), 
            'col_start' : int(match.group(2)),
            'row_start' : int(match.group(3)),
            'col_length' : int(match.group(4)),
            'row_length' : int(match.group(5))}, index = [0] ))


    df = pd.concat(parsed_claims, ignore_index=True)

    return df


def slice_for(claim): 
    return np.s_[claim.row_start:(claim.row_start + claim.row_length), claim.col_start:(claim.col_start+claim.col_length)]


def fabric_for(claims):
    fabric = np.zeros((FABRIC_EDGE, FABRIC_EDGE))

    for index,claim in claims.iterrows():
        this_claim = slice_for(claim)

        fabric[this_claim] = fabric[this_claim] + 1

    return fabric


def read_from(input_file):
    return pd.read_csv(input_file, header=None, names=['claim'], dtype=str, sep="\t")['claim']

class TestPart1(object):

    def test_example(self):
        input_string = """
#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2
"""
        num_overlapping = part_1(io.StringIO(input_string))

        assert num_overlapping == 4

class TestPart2(object):

    def test_examples(self):
        input_string = """
#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2
"""
        no_overlap_id = part_2(io.StringIO(input_string))

        assert no_overlap_id == 3

