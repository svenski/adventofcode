import pytest
import ipdb

import io
import re

import itertools
import numpy as np

from dataclasses import dataclass

from enum import Enum

def main():

    part_1('inputs/day_13.in')
    part_1('inputs/day_13.example')
    
    part_2('inputs/day_13.example.2')
    part_2('inputs/day_13.in')
    input_file = 'inputs/day_13.in'

def part_1(input_file):
    carts, roads, max_x, max_y = processBoard(input_file)

    iteration = 0
    collision = False

    # Note there is a bug here that is exposed in part 2. Have to check for collisions after each move!
    while not collision:
        cart_positions = set()
        for cc in carts:
            cc.advance(roads)
            if cc.pos in cart_positions:
                print(f'Collision at {cc.pos}')
                collision = True
            else:
                cart_positions.add(cc.pos)

        if iteration % 10 == 0:
            print(iteration)

        iteration += 1


def part_2(input_file):
    carts, roads, max_x, max_y = processBoard(input_file)

    def cart_sorting(cc):
        return cc.pos.y + max_y*cc.pos.x

    carts = sorted(carts, key=cart_sorting)
    
    iteration = 0
    collision = False

    while len(carts) > 1:
        cart_positions = set()
        collisions = set()
        for num,cc in enumerate(carts):
            if(cc.pos not in collisions):
                cc.advance(roads)
                if collides(cc.pos, carts):
                    collisions.add(cc.pos)

        carts = [cc for cc in carts if cc.pos not in collisions]
        iteration += 1

    return carts


def collides(pos, carts):
    num = 0
    for cc in carts:
        if cc.pos == pos:
            num +=1

    return num == 2


def processBoard(input_file):
    raw_board = []

    with open(input_file, 'r') as infile:
        lines = infile.read().split('\n')

    for ll in lines:
        if len(ll) > 0:
            raw_board.append(list(ll))

    board = np.array([np.array(xi) for xi in raw_board])

    carts = []
    roads = {}

    for (x,y), value in np.ndenumerate(board):
        rr, cc = parseRawBoard(value)

        if rr is not None:
            roads[Position(x,y)] = rr

        if cc is not None:
            carts.append(Cart(Position(x,y), cc, Turn.left))
            roads[Position(x,y)] = rr

    return carts, roads, x, y


def parseRawBoard(x):
    return {
            '>': (Road.horisontal, Direction.right),
            '^': (Road.vertical, Direction.up),
            '<': (Road.horisontal, Direction.left),
            'v': (Road.vertical, Direction.down), 
            '-': (Road.horisontal, None),
            '|': (Road.vertical, None),
            '/': (Road.diagonal_up_right, None),
            '\\': (Road.diagonal_up_left, None),
            '+': (Road.intersection, None),
            ' ': (None, None) }[x]


@dataclass(unsafe_hash=True, frozen=True)
class Position:
    x: int
    y: int

    def __add__(self,other):
        return Position(self.x + other.x, self.y + other.y)


class Direction(Enum):
    up = 0
    right = 1
    down = 2
    left = 3

    def nextXY(self):
        return { Direction.up : Position(-1,0),
                Direction.down: Position(1,0),
                Direction.left: Position(0, -1),
                Direction.right: Position(0,1) }[self]

    def turn_at_intersection(self, Turn):

        return { Turn.left : self.turn_left(),
                 Turn.right : self.turn_right(),
                 Turn.straight : self } [Turn]

    def turn_on_curve(self, road):
        if road == Road.diagonal_up_left:
            if self in set([Direction.up, Direction.down]):
                return self.turn_left()
            else:
                return self.turn_right()
        else:
            if self in set([Direction.up, Direction.down]):
                return self.turn_right()
            else:
                return self.turn_left()

    def turn_left(self):
        return Direction( (self.value -1) % 4)

    def turn_right(self):
        return Direction( (self.value +1) % 4)


class Turn(Enum):
    left = 0
    straight = 1
    right = 2
    
    def nextTurn(self):
        val = self.value
        return Turn((val +1) % 3)

class Road(Enum):
    horisontal = 1
    vertical = 2
    diagonal_up_right = 3
    diagonal_up_left = 4
    intersection = 5

@dataclass(unsafe_hash = True)
class Cart:
    pos: Position
    direction: Direction
    turn : Turn

    #TODO: Should live somewhere else
    def advance(self, roads):
        next_pos = self.pos + self.direction.nextXY()
        road_at_next_pos = roads[next_pos]
        next_direction = { 
                Road.horisontal : self.direction,
                Road.vertical : self.direction,
                Road.intersection : self.direction.turn_at_intersection(self.turn),
                Road.diagonal_up_right: self.direction.turn_on_curve(road_at_next_pos),
                Road.diagonal_up_left: self.direction.turn_on_curve(road_at_next_pos)}[road_at_next_pos]

        if road_at_next_pos == Road.intersection:
            self.turn = self.turn.nextTurn()

        self.pos = next_pos
        self.direction = next_direction
