import pytest

import io
import re

import itertools
import numpy as np


import pandas as pd

def main():

    input_file = 'inputs/day_12.txt'
    totals = part_1(input_file, 3000)

    
    df = pd.DataFrame({'t':np.array(totals)})
    df.plot()

    k = df.iloc[150] -df.iloc[149]
    m = df.iloc[150] - 150 *k


    x = 50000000000*k + m -k
    x = 2000*k + m
    df.iloc[2000]
    

def part_1(input_file, number_of_generations):
    state, activations = parse_input(input_file)

    compiled_patterns = {((re.escape(p))) for p in activations}
    compiled_patterns = {re.compile(insert_non_capture(p)) for p in compiled_patterns}

    generations = []
    start_pots = [0]
    generation_scores = []
    generations.append(state)
    start_pot = 0
    next_gen = state

    for gen in range(number_of_generations):
        next_gen, offset = calculate_next_generation_for(next_gen, compiled_patterns)
        start_pot = start_pot + offset
        generations.append(next_gen)
        start_pots.append(start_pot)
        generation_scores.append(score_for_generation(next_gen, start_pot))

    # visualise(generations, start_pots)
    return generation_scores



def score_for_generation(generation, start_pot):
    pat = r'\#'
    total = sum([m.start(0)  + start_pot for m in re.finditer(pat, generation)])
    return total


def visualise(generations, start_pots):

    min_start = min(start_pots)

    for n in range(len(generations)):
        prefix_with = start_pots[n] - min_start
        print( ' ' * prefix_with + generations[n])

def insert_non_capture(aa):
    n = 2
    return aa[:2] + '(?=' + aa[2:] + ')'


def calculate_next_generation_for(state, compiled_patterns):

    padded_previous_state, number_padded = padded_start_state(state)
    indexes_to_activate = []

    for cp in compiled_patterns:
        index_for_activation = [ m.start(0) + 2 for m in re.finditer(cp, padded_previous_state)]
        indexes_to_activate.append(index_for_activation)

    nn = list(itertools.chain(*indexes_to_activate))

    next_state = np.array(['.']*len(padded_previous_state))
    next_state[nn] = '#'

    return ''.join(next_state), number_padded

def find_first_hash(ss):
    p = r'\#'
    first_four = ss[:4]
    mm = re.search(p, first_four)
    if mm is None:
        return 4
    else:
        return mm.span()[0]

def padded_start_state(previous_state):
    # TODO: Could trim better
    prefix_pad_with = 4 - find_first_hash(previous_state[:4])
    postfix_pad_with = 4 - find_first_hash(previous_state[::-1][:4])


    return '.'*prefix_pad_with + previous_state + '.'*postfix_pad_with, -1*prefix_pad_with

def padded_start_state_orig(previous_state):
    prefix_pad_with = 4
    postfix_pad_with = 4

    return '.'*prefix_pad_with + previous_state + '.'*postfix_pad_with, -1*prefix_pad_with

def parse_input(input_file):

    with open(input_file, 'r') as infile:

        first_row = infile.readline()
        state = first_row[15:]
        state = state.strip()

        _ = infile.readline()

        transitions = infile.readlines()

        activations = set()
        for tt in transitions:
            pat, target = tt.split(' => ')
            target = target.strip()
            
            if target == '#':
                activations.add(pat.strip())

        return state, activations


class TestPart1(object):

    def test_example(self):
        input_string = """initial state: #..#.#..##......###...###

...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #
"""

        input_file = 'inputs/day_12.example.txt'
        pot_sums = part_1(input_file, 20)

        assert pot_sums[-1] == 325
