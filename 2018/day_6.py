import io
import pytest
import string

import pandas as pd
import numpy as np
import itertools

from scipy.spatial.distance import cdist


def main():
    input_file = 'inputs/day_6.csv'
    part_1(input_file)
    part_2(input_file, 10000)


def part_1(input_file):
    coords = read_from(input_file)

    max_col, max_row = coords.max() + (1,1)
    area_coords = np.array(get_area_coords(max_col, max_row))
    stacked_distances = calculate_individual_distances_for(coords)

    # minimums per coord
    minimum_value_at_coords = np.min(stacked_distances, axis=0)

    # counts at each to remove clashes.
    pp = np.argwhere(stacked_distances == minimum_value_at_coords)
    squares_represented_with_min = pp[:,1]
    s = np.sort(squares_represented_with_min)
    index_of_dupes = s[:-1][s[1:] == s[:-1]]

    minimum_coord = np.argmin(stacked_distances, 0)
    # Cannot convert float NaN to integer
    minimum_coord[index_of_dupes] = -1

    # find number at edges
    col_edges =(area_coords[:,0] == 0) | (area_coords[:,0] == (max_col -1))
    row_edges =(area_coords[:,1] == 0) | (area_coords[:,1] == (max_row -1))
    all_edges = col_edges | row_edges
    coord_nums_at_edges = np.unique(minimum_coord[all_edges])
    coord_nums_at_edges = coord_nums_at_edges

    remaining_coord_nums = minimum_coord[~np.isin(minimum_coord, coord_nums_at_edges)]
    import collections
    counts = collections.Counter(remaining_coord_nums)
    del counts[-1]
    return counts.most_common(1)[0][1]


def part_2(input_file, threshold = 32):
    coords = read_from(input_file)

    stacked_distances = calculate_individual_distances_for(coords)
    total_distance_for_all = np.sum(stacked_distances, axis = 0)

    return len(total_distance_for_all[total_distance_for_all < threshold])


def calculate_individual_distances_for(coords):
    max_col, max_row = coords.max() + (1,1)

    number_of_coordinates = len(coords)

    area_coords = np.array(get_area_coords(max_col, max_row))

    stacked_distances = np.zeros((number_of_coordinates, max_col * max_row)) -1
    
    for index, cc in coords.iterrows():
        dst = cdist(area_coords, [[cc.col, cc.row]], 'cityblock')
        stacked_distances[index] = dst.flatten()

    return stacked_distances


def get_area_coords(n_cols, n_rows):
    return list(itertools.product(np.arange(n_cols), np.arange(n_rows)))

def read_from(input_file):
    return pd.read_csv(input_file, header=None, names=['col', 'row'])

class TestPart1(object):

    def test_example(self):
        input_string = """
1, 1
1, 6
8, 3
3, 4
5, 5
8, 9
        """
        out = part_1(io.StringIO(input_string))

        assert out == 17



