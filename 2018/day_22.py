import pytest

import itertools
import numpy as np
import io
import re

from aoc import timeit

def main():
    input_file = 'inputs/day_22.txt'
    part_1(input_file)
    


def part_1(infile):
    region_types = parse_input(infile)
    total = region_types.sum()
    return total


@timeit
def part_2(infile):
    # TODO: Have to expand the selection
    region_types = parse_input(infile)

    starting_point = (0,0)
    arrived = False

    tool_directions = []

    while not arrived:



def parse_input(infile):
    rows = open(infile,'r').readlines()
    rr = re.match('depth: (\d+)$', rows[0])
    depth = int(rr.group(1))
    target = list(map(lambda x: int(x) + 1,re.match('target: (\d+,\d+)$', rows[1]).group(1).split(',')))
    
    erosion_level = np.zeros(shape = target)
    mm = 20183

    erosion_level[0,0] = depth % mm

    for (x,y),val in np.ndenumerate(erosion_level):
        if x == 0:
            erosion_level[x,y] = (y * 48271 + depth) % mm
        elif y == 0:
            erosion_level[x,y] = (x * 16807 + depth) % mm
        else:
            erosion_level[x,y] = (erosion_level[x-1,y]*erosion_level[x,y-1] + depth) % mm

    erosion_level[target[0]-1, target[1]-1] = depth % mm
    
    region_types = erosion_level % 3
    return region_types


class TestPart1(object):

    def test_example(self):
        infile = 'inputs/day_22.example.txt'
        
        assert part_1(infile) == 114



class TestPart2(object):


    def test_hero_moves(self):
        # Given a board with two squares



    def test_example(self):
        None

