import pandas as pd
import numpy as np

import pytest
from collections import Counter

import io

import Levenshtein

def main():

    input_file = 'inputs/day_2.csv'

    part_1(input_file)

    part_2(input_file)[0]


def part_1(input_file):
    ids = read_from(input_file)

    twos = 0
    threes = 0

    for this_id in ids:
        counter = Counter(this_id)
        values = set(counter.values())
        if 3 in values: threes +=1
        if 2 in values: twos +=1

    return twos * threes


def part_2(input_file):

    ids = read_from(input_file)
    
    candidates = []
    for this_id in ids:
        for that_id in ids:
            if Levenshtein.distance(this_id, that_id) == 1:
                mb = Levenshtein.matching_blocks(Levenshtein.editops(this_id,that_id), this_id, that_id)
                common = ''.join([this_id[x[0]:x[0]+x[2]] for x in mb])
                candidates.append(common)


    return candidates


def read_from(input_file):
    return pd.read_csv(input_file, header=None, names=['ids'], dtype=str)['ids']

class TestPart2(object):

    def test_example(self):
        input_string = """
abcde
fghij
klmno
pqrst
fguij
axcye
wvxyz"""

        out = part_2(io.StringIO(input_string))[0]
        assert out == 'fgij'

class TestPart1(object):

    def test_example(self):
        input_string = """
        abcdef
        bababc
        abbcde
        abcccd
        aabcdd
        abcdee
        ababab"""

        assert part_1(io.StringIO(input_string)) == 12
