import pytest
import ipdb

import io
import re

import itertools
import numpy as np

from dataclasses import dataclass
from dataclasses import field

from typing import List
from typing import Set

from collections import defaultdict

from enum import Enum

import logging
import ipdb

def main():
    None


def part_1(input_string):

    logging.basicConfig(format='%(message)s', level=logging.INFO)

    board = parse_input(input_string)
    logging.info(f'Starting board\n{repr(board)}')

    round = 1
    while board.game_is_on():
        next_round(board)
        round +=1

    logging.info(f'Final board: \n{board}')
    logging.info(f'Number of rounds: {round}')

    remaining = board.creatures_in_reading_order()
    hps = map(lambda x: x.hp, remaining)
    logging.info(f'Sum of remaining hps: {sum(hps)}')
    logging.info(f'Remaining players: {board.print_creatures()}')

    return (round -1)*sum(hps)


def next_round(board):
    creatures = board.creatures_in_reading_order()
    for creature in creatures:

        logging.debug(f'Processing {creature}')
        # move
        board.find_next_move(creature)
        board.attack(creature)



    logging.debug(f'Board \n{repr(board)}')
    logging.debug(f'Remaining players: \n{board.print_creatures()}')
     


def parse_input(input_string):
    rows = list(map(lambda x: x.strip(), filter(lambda x: len(x) > 0, input_string.split('\n'))))
    rows = [list(x) for x in rows]

    row_nums = len(rows)
    col_nums = len(rows[0])

    elves = set()
    goblins = set()
    walls = set()

    for rr in range(row_nums):
        for cc in range(col_nums):
            entry = rows[rr][cc]
            pos = Position(rr,cc)

            if entry == '#':
                walls.add(pos)
            elif entry == 'E':
                elves.add(Elf(pos))
            elif entry == 'G':
                goblins.add(Goblin(pos))

    return Board(row_nums, col_nums, elves, goblins, walls)
    
    

def getPositions(creates):
    return list(map(x.pos, creates))


def part_2(input_file):
    None


@dataclass(unsafe_hash=True, frozen=True)
class Position:
    x: int
    y: int

    def up(self):
        return Position(self.x - 1, self.y)

    def down(self):
        return Position(self.x + 1, self.y)

    def left(self):
        return Position(self.x, self.y - 1)

    def right(self):
        return Position(self.x, self.y + 1)

    def all_directions(self):
        return [self.up(), self.right(), self.down(), self.left()]


@dataclass
class Creature:
    pos: Position 
    hp: int = field(default=200, init=False, hash=False)

    def distance_to_pos(self, p):
        return np.abs(self.x - p.x) + np.abs(self.y - p.y)
        

@dataclass
class Elf(Creature):
    idCounter = 0

    def __init__(self, pos):
        super().__init__(pos)
        Elf.idCounter +=1
        self.id = Elf.idCounter

    def __repr__(self):
        return f'Elf(id {self.id}) at {self.pos} with {self.hp} hp'

    def __hash__(self):
        return hash(self.id)


@dataclass
class Goblin(Creature):
    idCounter = 0

    def __init__(self, pos):
        super().__init__(pos)
        Goblin.idCounter +=1
        self.id = Goblin.idCounter

    def __repr__(self):
        return f'Goblin(id {self.id}) at {self.pos} with {self.hp} hp'

    def __hash__(self):
        return hash(self.id)


@dataclass
class Wall:
    pos: Position


@dataclass
class Board:
    rows : int
    cols : int
    elves: Set[Elf]
    goblins: Set[Goblin]
    walls: Set[Position]

    def game_is_on(self):
         return (len(self.goblins) > 0) and (len(self.elves) > 0)

    def creatures_in_reading_order(self):
        return sorted(self.elves.union(self.goblins), key=lambda x: x.pos.x*self.cols + x.pos.y)
    
    def print_creatures(self):
        return '\n'.join(list(map(repr,self.creatures_in_reading_order())))
        

    def creatures_in_weakest_order(self, creatures):
        return sorted(creatures, key=lambda x: x.hp)

    def positions_in_reading_order(self, xs):
        return sorted(xs, key=lambda x: x.x*self.cols + x.y)
    

    def attack(self, creature):
        adversaries = self.elves if isinstance(creature, Goblin)  else self.goblins

        initial_positions = creature.pos.all_directions()

        adjacent_adversaries = list(filter(lambda x: x.pos in initial_positions, adversaries))

        if len(adjacent_adversaries)> 0:
            if len(adjacent_adversaries) > 1:
                weakest = self.creatures_in_weakest_order(adjacent_adversaries)[0]
            else:
                weakest = adjacent_adversaries[0]

            logging.debug(f'	ATTACK: {weakest}')
            weakest.hp = weakest.hp - 3

            if weakest.hp <= 0:
                logging.debug(f'{weakest} is killed!')
                adversaries.remove(weakest)

        else:
            None
            # logging.debug('	ATTACK: No adversaries close enough')

    def find_next_move(self,creature):

        adversaries = self.elves if isinstance(creature, Goblin)  else self.goblins
        friendlies = self.goblins if isinstance(creature, Goblin)  else self.elves

        pp = creature.pos
        excluded = self.walls.copy()
        excluded.add(pp)
        excluded.update(Board.to_positions(friendlies))

        initial_positions = creature.pos.all_directions()

        if len(set(Board.to_positions(adversaries)).intersection(initial_positions)) > 0:
            # logging.debug('	MOVE: No move. Found adjacent adversary.')
            return None

        valid_directions = {p:[] for p in initial_positions if p not in excluded}
        found_adversary = []

        while len(valid_directions) > 0:
            for dd,path in valid_directions.items():
                # logging.trace(f'Checking pos: {dd} with path: {path}')
                if dd in Board.to_positions(adversaries):
                    path_to_adv = path.copy()
                    path_to_adv.append(dd)
                    found_adversary.append(path_to_adv)
        
            if len(found_adversary) > 0:
                # logging.trace(f'Found adversaries at paths: {found_adversary}')
                break
            else:
                excluded = excluded.union(set(valid_directions))
                new_valid = {}
                for dd, path in valid_directions.items():
                    next_positions = dd.all_directions()
                    path_so_far = path.copy() + [dd]
                    next_valid_dirs = set(next_positions) - excluded
                    next_valid = {p:path_so_far for p in next_valid_dirs}

                    if next_valid is not None:
                        if len(next_valid) > 0:
                            # logging.trace(f'Adding {next_valid} as next valid')
                            new_valid.update(next_valid.copy())

                # ipdb.set_trace()

                valid_directions = new_valid

        
        if len(found_adversary) > 0:
            first_steps = [nn[0] for nn in found_adversary]
            first_steps = self.positions_in_reading_order(first_steps)
            logging.debug(f'	MOVE: {first_steps[0]}')
            creature.pos = first_steps[0]
        else:
            None
            # logging.debug(f'	MOVE: No move')

            

    @staticmethod
    def to_positions(creatures):
        return set(map(lambda x: x.pos, creatures))

    def __repr__(self):

        row_string = ''
        for rr in range(self.rows):
            for cc in range(self.cols):
                p = Position(rr, cc)
                if p in self.walls:
                    row_string = row_string + '#'
                elif p in Board.to_positions(self.goblins):
                    row_string = row_string + 'G'
                elif p in Board.to_positions(self.elves):
                    row_string = row_string + 'E'
                else:
                    row_string = row_string + '.'

            row_string = row_string + '\n'
            
        return row_string



class TestPart1(object):

    def test_examples(self):

        input_string = """#######
#G..#E#
#E#E.E#
#G.##.#
#...#E#
#...E.#
#######
"""
        assert part_1(input_string) == 36334

        input_string="""####### 
#.G...#
#...EG#
#.#.#G#
#..G#E#
#.....#
#######
"""
        assert part_1(input_string) == 27730
