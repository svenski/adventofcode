from dataclasses import dataclass
from dataclasses import field
from typing import Any

import typing

import re

from collections import defaultdict

import time

def main():
    
    input_string = "448 players; last marble is worth 71628 points"
    part_1(input_string)
    input_string = "448 players; last marble is worth 7162800 points"
    part_1(input_string)
    

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        print('%r  %2.2f ms' % (method.__name__, (te - ts) * 1000))
        return result
    return timed

@timeit
def part_1(input_string):

    number_of_players, number_of_marbles = parse_input(input_string)

    player_scores = defaultdict(int)
    m = MarbleRing(Ring(Node(0)))

    for nn in range(number_of_marbles):
        nn += 1
        player_number = (nn % number_of_players) + 1
        player_scores[player_number] += m.play_next_marble()

    return sorted(player_scores.values(), reverse=True)[0]


def parse_input(input_string):
    pattern = r'(\d+) players; last marble is worth (\d+) points'
    mm = re.match(pattern, input_string)
    number_of_players = int(mm.group(1))
    number_of_marbles = int(mm.group(2))

    return number_of_players, number_of_marbles


@dataclass
class Node:
    val : int
    previous : Any = field(repr=False, init=False)
    nexxt: Any = field(repr=False, init=False)

@dataclass
class Ring:
    current: Node
    count: int = 1

    def __post_init__(self):
        nn = self.current
        nn.nexxt = nn
        nn.previous = nn

    def insert(self,nn: Node):
        nn.nexxt = self.current.nexxt
        nn.previous = self.current

        self.current.nexxt.previous = nn
        self.current.nexxt = nn

        self.current = nn
        self.count +=1
        return self


    def pop(self):

        if self.current.nexxt == self.current:
            raise ValueError('Cannot delete last node in the ring')

        cc = self.current
        pp = self.current.previous
        nn = self.current.nexxt

        pp.nexxt = nn
        nn.previous = pp
        self.current = nn
        self.count -=1
        
        return cc

    def next(self):
        self.current = self.current.nexxt
        return self

    def previous(self):
        self.current = self.current.previous
        return self



@dataclass
class MarbleRing:
    ring: Ring #= field(default=Ring(Node(0))) Caused it to reuse the previous object!
    current_marble : int = field(init=False, default=0)

    def play_next_marble(self):
        self.current_marble +=1

        if self.current_marble % 23 == 0:
            return self.ring.previous().previous().previous().previous().previous().previous().previous().pop().val + self.current_marble
        else:
            self.ring.next().insert(Node(val = self.current_marble))
            return 0


    def __repr__(self):
        rr = self.ring
        current_node = rr.current

        repl = str(current_node)
        current_node = rr.current.nexxt


        while(current_node != rr.current):
            repl += f' :-: {str(current_node)}'
            current_node = current_node.nexxt

        return repl


class TestRing(object):
    
    def can_create_ring_with_one_node(self):
        node_0 = Node(3)
        rr = Ring(current = node_0)


        assert rr.current.nexxt == rr.current
        assert rr.current.previous == rr.current


    def can_create_ring_with_two_nodes(self):

        node_0 = Node(3)
        node_1 = Node(4)

        rr = Ring(current=node_0)
        rr.insert(node_1)

        assert rr.current.nexxt.nexxt == rr.current
        assert rr.current.nexxt == rr.current.previous

    def can_remove_ring(self):

        node_0 = Node(3)
        node_1 = Node(4)

        rr = Ring(current=node_0)
        rr.insert(node_1)

        rr.pop()
        assert rr.current.nexxt == rr.current
        assert rr.current.previous == rr.current
        

    def can_move_to_next(self):
        node_0 = Node(3)
        node_1 = Node(4)

        rr = Ring(current=node_0)
        rr.insert(node_1)
        a = rr.current.nexxt 

        assert rr.next() == a



class TestExamples(object):

    def test_example(self):
        input_string = """10 players; last marble is worth 1618 points"""
        out = part_1(input_string)

        assert out == 8317

    def test_example2(self):
        input_string = """13 players; last marble is worth 7999 points"""
        out = part_1(input_string)

        assert out == 146373





########

from collections import deque, defaultdict

@timeit
def play_game(max_players, last_marble):
    scores = defaultdict(int)
    circle = deque([0])

    for marble in range(1, last_marble + 1):
        if marble % 23 == 0:
            circle.rotate(7)
            scores[marble % max_players] += marble + circle.pop()
            circle.rotate(-1)
        else:
            circle.rotate(-1)
            circle.append(marble)

    return max(scores.values()) if scores else 0
