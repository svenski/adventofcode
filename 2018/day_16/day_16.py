
import pytest
import ipdb

import io
import re

import itertools
import numpy as np

from dataclasses import dataclass
from dataclasses import field

from typing import List

def main():
    
    part_1('./input.txt')
    out = part_1('input_example.txt')

def part_1(input_file):
    executions = parse_input(input_file)
    ipdb.set_trace()
    ops = opcodes()
    matches = []

    for name, args in ops.items():
        print(f'Applying {name}')
        after = apply_function_generic(executions[0].before, executions[0].instruction, **args)
        if after == executions[0].after:
            matches.append(name)



def opcodes():
    ops = { 
            'addr': 
            {
                'operand': int.__add__,
                'immediate_a': False,
                'immediate_b': False,
                'unary' : False
                },
            'addi': 
            {
                'operand': int.__add__,
                'immediate_a': False,
                'immediate_b': True,
                'unary' : False
                },
            'mulr': 
            {
                'operand': int.__mul__,
                'immediate_a': False,
                'immediate_b': False,
                'unary' : False
                },
            'muli': 
            {
                'operand': int.__mul__,
                'immediate_a': False,
                'immediate_b': True,
                'unary' : False
                },
            'banr': 
            {
                'operand': int.__and__,
                'immediate_a': False,
                'immediate_b': False,
                'unary' : False
                },
            'bani': 
            {
                'operand': int.__and__,
                'immediate_a': False,
                'immediate_b': True,
                'unary' : False
                },
            'borr': 
            {
                'operand': int.__or__,
                'immediate_a': False,
                'immediate_b': False,
                'unary' : False
                },
            'bori': 
            {
                'operand': int.__or__,
                'immediate_a': False,
                'immediate_b': True,
                'unary' : False
                },
            'setr': 
            {
                'operand': int.__int__,
                'immediate_a': False,
                'unary' : True
                },
            'seti': 
            {
                'operand': int.__int__,
                'immediate_a': True,
                'unary' : True
                },
            'gtrr': 
            {
                'operand': int.__gt__,
                'immediate_a': False,
                'immediate_b': False,
                'unary' : False
                },
            'gtri': 
            {
                'operand': int.__gt__,
                'immediate_a': False,
                'immediate_b': True,
                'unary' : False
                },
            'gtir': 
            {
                'operand': int.__gt__,
                'immediate_a': True,
                'immediate_b': False,
                'unary' : False
                },
            'eqrr': 
            {
                'operand': int.__eq__,
                'immediate_a': False,
                'immediate_b': False,
                'unary' : False
                },
            'eqri': 
            {
                'operand': int.__eq__,
                'immediate_a': False,
                'immediate_b': True,
                'unary' : False
                },
            'eqir': 
            {
                'operand': int.__eq__,
                'immediate_a': True,
                'immediate_b': False,
                'unary' : False
                }
            }
    return ops



def apply_function_generic(registers, 
        instruction,
        operand, 
        immediate_a, 
        immediate_b = False, 
        unary = False):

    if immediate_a:
        a = instruction.a
    else:
        a = registers[instruction.a]

    if immediate_b:
        b = instruction.b
    else:
        b = registers[instruction.b]

    if unary:
        out = operand(a)
    else:
        out = operand(a, b)

    return assemble_out(registers, instruction, out)


def assemble_out(registers, instruction, out):
    reg_after = registers.regs.copy()
    reg_after[instruction.c] = out

    return Registers([int(x) for x in reg_after])

def parse_input(input_file):

    start_of_exec = r'^Before:'
    reg_reg = re.compile('\[(\d), (\d), (\d), (\d)\]')

    with open(input_file) as infile:
        all_data = infile.readlines()

        executions = []

        for ii in range(len(all_data)):
            line = all_data[ii]
            if re.match(start_of_exec, line):
                before = parse_registers(line, reg_reg)
                instruction = parse_instruction(all_data[ii + 1])
                after = parse_registers(all_data[ii + 2], reg_reg)

                executions.append(Execution(before, instruction, after))

    return executions


def parse_registers(line, reg):
    rr = [int(r) for r in reg.findall(line)[0]]
    return Registers(rr)


def parse_instruction(line):
    return Instruction(*[int(dig) for dig in line.split(" ")])


def default_registers():
    return [0,0,0,0]


@dataclass
class Registers:
    regs :List[int] = field(default_factory = default_registers)

    def __getitem__(self, key):
        return self.regs[key]

    def __setitem__(self, key, value):
        self.regs[key] = value


@dataclass
class Instruction:
    opcode: int
    a: int
    b: int
    c: int
        
@dataclass
class Execution:
    before: Registers
    instruction: Instruction
    after: Registers

class TestPart1(object):

    def test_example(self):

        out = part_1('input_example.txt')

        assert out == 'CABDFE'
