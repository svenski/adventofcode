import numpy as np
import pandas as pd
import io

def main():
    input_file = 'inputs/day_1_input.csv'

    part_1(input_file)
    part_2(input_file)

def part_1(input_file):
    return read_from(input_file).sum()


def part_2(input_file):

    freq_delta = read_from(input_file).values

    len_freqs = len(freq_delta)
    i = 0

    freqs_visited = set()
    current_freq = 0

    while True:
        current_freq = current_freq + freq_delta[i % len_freqs]
        if current_freq in freqs_visited:
            break

        freqs_visited.add(current_freq)
        i +=1


    print(f'Current freq: {current_freq} \ni: {i}')
    return current_freq


def read_from(input_file):
    return pd.read_csv(input_file, header=None, names=['freq'], dtype=int)['freq']


class TestPart2(object):
        
    def test_example_1(self):
        input_string = """
+1
-2
+3
+1
"""
        assert part_2(io.StringIO(input_string)) == 2


    def test_example_2(self):
        input_string = """
    +3
    +3
    +4
    -2
    -4
"""
        assert part_2(io.StringIO(input_string)) == 10

    def test_example_3(self):
        input_string = """
        +7
        +7
        -2
        -7
        -4
"""
        assert part_2(io.StringIO(input_string)) == 14

    



class TestPart1(object):
    def test_read_from_file_and_sum(self):
        input_file = 'inputs/day_1_input.csv'
        assert part_1(input_file) == 525

    def test_read_from_string_io(self):
        input_string = """
+1
-2
+3
"""
        assert part_1(io.StringIO(input_string)) == 2

