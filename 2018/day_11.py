import pandas as pd
import numpy as np

import pytest

import scipy.signal

def main():
    part_1(6548)
    part_2(6548)

def part_1(input_string, conv_filter_size = 3):

    serial = int(input_string)
    
    c = np.arange(300) + 1

    cn = np.reshape(np.tile(c, 300), (300,300))
    rn = np.transpose(cn)

    rack_id = rn + 10

    ii  = np.multiply((np.multiply(rack_id, cn) + serial), rack_id)
    power_level = ((ii % 1000)/100).astype(int)-5

    sum_conv = np.ones((conv_filter_size,conv_filter_size))

    out = scipy.signal.convolve2d(power_level, sum_conv, mode='valid')
    ind = np.unravel_index(np.argmax(out), out.shape)
    power = out[ind]
    return np.array(ind) + 1, power

def part_2(input_string):

    outs = []
    for conv_size in np.arange(300) + 1:
        if conv_size % 30 == 0:
            print(conv_size)
        ind, power = part_1(input_string, conv_size)
        outs.append((*ind, conv_size, power))

        if power < 0: break

    df = pd.DataFrame(outs, columns=['x','y','conv','power'])

    outs.sort(key = lambda x: x[3],reverse =True)
    return outs[0][:3]


class TestPart1(object):

    def test_example(self):
        input_string = "18"
        ind, power = part_1(input_string)

        assert (ind == np.array([33,45])).all()


class TestPart(object):

    def test_example(self):
        input_string = "18"

        out = part_2(input_string)

        assert (out == np.array([33,45, 16])).all()
