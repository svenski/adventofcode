import pandas as pd
import numpy as np

import io
import pytest

import re

def main():

    input_file = 'inputs/day_4.txt'
    part_1(input_file)
    part_2(input_file)

def part_1(input_file):
    log = read_from(input_file)
    guard_data = parse(log)
    all_minute_data = to_guard_minute_data(guard_data)

    sleep_per_guard = (all_minute_data.groupby('guard').size().to_frame('minutes').sort_values('minutes', ascending = False).reset_index())
    sleepiest_guard = sleep_per_guard.guard.values[0]

    minute_with_most_sleep = (all_minute_data[all_minute_data['guard'] == sleepiest_guard].
            groupby('minute').
            size().to_frame('total_sleep_in_minute').
            sort_values('total_sleep_in_minute', ascending=False).
            reset_index().
            minute.values[0])

    return int(sleepiest_guard)*minute_with_most_sleep


def part_2(input_file):
    log = read_from(input_file)
    guard_data = parse(log)
    all_minute_data = to_guard_minute_data(guard_data)

    sleep_per_guard_minute = (all_minute_data.groupby(['guard','minute']).size().to_frame('count').sort_values('count', ascending = False).reset_index())

    return int(sleep_per_guard_minute.guard.values[0])*sleep_per_guard_minute.minute.values[0]


def to_guard_minute_data(guard_data):

    guard_data = guard_data.sort_values(['date','hour','minute'])
    guard_data['guard'] = guard_data['guard'].fillna(method = 'pad')
    guard_data = guard_data[guard_data['action'] != 'begins shift'].copy()
    total_sleeps = len(guard_data)/2
    guard_data['sleep_id'] = np.repeat(np.arange(total_sleeps),2)

    minute_data = []

    for sid, dd in guard_data.groupby('sleep_id'):
        minute_data.append(pd.DataFrame({'guard' : dd.guard.values[0], 'minute' : np.arange(dd.minute.values[0], dd.minute.values[1])}))

    all_minute_data = pd.concat(minute_data, ignore_index=True)

    return all_minute_data

def parse(log):

    parsed_entries = []
    pattern = r"\[(\d{4}-\d{2}-\d{2}) (\d{2}):(\d{2})\](?: Guard #(\d+))? (.+)$"

    for entry in log:
        match = re.search(pattern, entry)
        parsed_entries.append(pd.DataFrame({
            'date': match.group(1), 
            'hour' : int(match.group(2)),
            'minute' : int(match.group(3)),
            'guard' : match.group(4),
            'action' : match.group(5)}, index = [0] ))

    df = pd.concat(parsed_entries, ignore_index=True)

    return df


def read_from(input_file):
    return pd.read_csv(input_file, header=None, names=['data'], dtype=str, sep="\t")['data']


############################################
# Tests
class TestPart1(object):

    def test_example(self):
        input_string = """
[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up
"""
        out = part_1(io.StringIO(input_string))

        assert out == 240

class TestPart2(object):

    def test_example(self):

        input_string = """
[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up
"""
        out = part_2(io.StringIO(input_string))

        assert out == 4455
