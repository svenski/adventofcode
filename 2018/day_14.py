import pytest

import itertools
import numpy as np

from aoc import timeit

def main():
    part_1(846021)
    part_2('846021')


def part_1(end_rec):

    recipies = [3,7]
    elves_at = [0,1]
    length = 10

    while len(recipies) < (end_rec + length):
        elv_vals = [ recipies[x] for x in elves_at]
        next_values = list(map(int, list(str(sum(elv_vals)))))
        recipies.extend(next_values)

        elves_at = [(index + recipies[index] + 1) % len(recipies) for index in elves_at]

    return ''.join(map(str,recipies[end_rec:(end_rec + length)]))


@timeit
def part_2(match):

    end_rec = 100000000
    recipies = [3,7]
    elves_at = [0,1]
    length = 10

    while len(recipies) < (end_rec + length):
        elv_vals = [ recipies[x] for x in elves_at]
        next_values = list(map(int, list(str(sum(elv_vals)))))
        recipies.extend(next_values)

        elves_at = [(index + recipies[index] + 1) % len(recipies) for index in elves_at]

    s = ''.join(map(str,recipies))
    return s.find(match)


class TestPart1(object):

    def test_example(self):

        assert part_1(9) == '5158916779'
        assert part_1(5) == '0124515891'
        assert part_1(18) == '9251071085'
        assert part_1(2018) == '5941429882'


class TestPart2(object):
    def test_example(self):

        assert part_2('51589') == 9
        assert part_2('01245') == 5
        assert part_2('92510') ==18
        assert part_2('59414') ==2018

