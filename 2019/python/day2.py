import pytest

import operator


def part_1(input_file, live=False):
    program = list_from_file(input_file)
    return run_program(program, live)


def run_program(program, live=False, noun=12, verb = 2):

    if live:
        program[1]=noun
        program[2]=verb

    current_position = 0

    while (opcode := program[current_position]) != 99:
        # print(f'{current_position=}, {opcode=}')
        input_1_pos = program[current_position + 1]
        input_2_pos = program[current_position + 2]
        output_pos = program[current_position + 3]

        if opcode == 1:
            operand = operator.add
        elif opcode == 2:
            operand = operator.mul
        else:
            raise ValueError(f'The opcode is not a valid value: {opcode}')

        # print(f'Using {operand} on position {input_1_pos} (value: {program[input_1_pos]}) and position {input_2_pos} (value: {program[input_2_pos]}) to output in pos {output_pos}')
        program[output_pos] = operand(program[input_1_pos] , program[input_2_pos])
        current_position = current_position + 4

    return program


def part_2(input_file):

    desired_output = 19690720
    program = list_from_file(input_file)

    max_input = len(program)

    noun, verb = find_desired(program, max_input, desired_output)

    return 100*noun + verb

def find_desired(program, max_input, desired_output):

    for noun in range(0, max_input):
        for verb in range(0, max_input):
            output = run_program(program[:], live=True, noun=noun, verb=verb)[0]
            if  output == desired_output:
                print(f'Found the desired output: {noun=}, {verb=})')
                return noun, verb
            else:
                print(f'{output=}')


    
    raise ValueError('Could not find desired output!')


def list_from_file(input_file):

    with open(input_file, 'r') as infile:
        ll = [int(a) for a in infile.readline().split(',')]

    return ll


def save_test_file(input_string, day):
    test_file = f"../inputs/day_{day}_test.txt"

    with open(test_file, 'w') as output:
        output.write(input_string)

    return test_file


class TestPart1(object):

    def test_example(self):
        input_string = """1,0,0,0,99"""
        assert part_1( save_test_file(input_string, 2)) == [2,0,0,0,99]

        assert part_1( save_test_file("1,1,1,4,99,5,6,0,99", 2)) == [30,1,1,4,2,5,6,0,99]

        assert part_1( save_test_file("1,9,10,3,2,3,11,0,99,30,40,50", 2))  == [3500,9,10,70, 2,3,11,0, 99, 30,40,50] 

    def part_part(self):
        print(part_1('../inputs/day_2.txt', live=True))


    def test_part2():
        input_file = '../inputs/day_2.txt'
        print(part_2(input_file))


