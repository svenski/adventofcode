import pytest

import operator

from aoc import save_test_file
from aoc import print_and_assert

import logging


def part_1(inputfile):
    
    program = list_from_file(inputfile)
    logging.debug(program[:30])
    return run_program(program, input_num=1)

def part_2(inputfile, input_num):
    program = list_from_file(inputfile)
    return run_program(program, input_num=input_num)

def binary_operation_for(program, current_position, operand, param1_mode, param2_mode):
    input1_pos = program[current_position + 1]
    input2_pos = program[current_position + 2]
    output_pos = program[current_position + 3]
    logging.debug(f'{input1_pos=}, {param1_mode=}, {input2_pos=}, {param2_mode=}, {output_pos=}')
    input1 = input1_pos if param1_mode  == 1 else program[input1_pos]
    logging.debug(f'{input2_pos=}, {param2_mode=}')
    input2 = input2_pos if param2_mode == 1 else program[input2_pos]
    program[output_pos] = operand(input1, input2)

    return 4

def unary_test_for(program, current_position, operand, param1_mode, param2_mode):
    input1_pos = program[current_position + 1]
    input2_pos = program[current_position + 2]
    input1 = input1_pos if param1_mode == 1 else program[input1_pos]
    input2 = input2_pos if param2_mode == 1 else program[input2_pos]

    if operand(input1, 0):
        return input2, 0
    else:
        return current_position, 3

def binary_comparison_for(program, current_position, operand, param1_mode, param2_mode):
    input1_pos = program[current_position + 1]
    input2_pos = program[current_position + 2]
    output_pos = program[current_position + 3]

    input1 = input1_pos if param1_mode == 1 else program[input1_pos]
    input2 = input2_pos if param2_mode == 1 else program[input2_pos]

    if operand(input1, input2):
        program[output_pos] = 1
    else:
        program[output_pos] = 0

    return 4


def parse_from(op_instr):
    op_str = f'{op_instr:05}'
    opcode = int(op_str[-2:])
    param1_mode = int(op_str[2])
    param2_mode = int(op_str[1])
    return opcode, param1_mode, param2_mode


def run_program(program, input_num):

    current_position = 0
    opcode, param1_mode, param2_mode = parse_from(program[current_position])

    advance = None

    while opcode != 99:
        if opcode == 1:
            advance = binary_operation_for(program, current_position, operator.add, param1_mode, param2_mode)
        elif opcode == 2:
            advance = binary_operation_for(program, current_position, operator.mul, param1_mode, param2_mode)
        elif opcode == 3:
            print(f'simulating input: {input_num}')
            output_pos = program[current_position + 1]
            program[output_pos] = input_num
            advance = 2
        elif opcode == 4:
            input1_pos = program[current_position + 1]
            input1 = input1_pos if param1_mode else program[input1_pos]
            print(f'output from program: {input1}')
            advance = 2
        elif opcode == 5:
            current_position, advance = unary_test_for(program, current_position, operator.ne, param1_mode, param2_mode)

        elif opcode == 6:
            current_position, advance = unary_test_for(program, current_position, operator.eq, param1_mode, param2_mode)
        elif opcode == 7:
            advance = binary_comparison_for(program, current_position, operator.lt, param1_mode, param2_mode)

        elif opcode == 8:
            advance = binary_comparison_for(program, current_position, operator.eq, param1_mode, param2_mode)

        else:
            raise ValueError(f'The opcode is not a valid value: {opcode}')

        
        current_position = current_position + advance

        logging.debug(f'-----------------------------------------------------------------')
        logging.debug(f'Next program instruction: {program[current_position]} at {current_position=}')
        opcode, param1_mode, param2_mode = parse_from(program[current_position])
        print(f'{ opcode= }, { param1_mode= }, { param2_mode= }, {current_position=}')
        print(f'{program}')
        logging.debug(f'{opcode=}, {param1_mode=}, {param2_mode=}')





def list_from_file(inputfile):
    with open(inputfile, 'r') as infile:
        ll = [int(a) for a in infile.readline().split(',')]

    return ll




def test_example():
    inputstring = """1,0,0,0,99"""
    assert part_1( save_test_file(inputstring, 2)) == [2,0,0,0,99]
    assert part_1( save_test_file("1,1,1,4,99,5,6,0,99", 2)) == [30,1,1,4,2,5,6,0,99]
    assert part_1( save_test_file("1,9,10,3,2,3,11,0,99,30,40,50", 2))  == [3500,9,10,70, 2,3,11,0, 99, 30,40,50] 

    print_and_assert(part_2(save_test_file("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9", 5), 0), 0)
    print_and_assert(part_2(save_test_file("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9", 5), 3), 1)


def part1():
    inputfile = '../inputs/day_5.txt'
    print(part_1(inputfile))


def main_part2():
    inputfile = '../inputs/day_5.txt'
    part_2(inputfile, 5)


