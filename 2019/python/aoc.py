import tempfile
import os
import time

def save_test_file(input_string):

    fd, path = tempfile.mkstemp()
    try:
        with os.fdopen(fd, 'w') as tmp:
            tmp.write(input_string)
    except Exception as e:
        print(e)

    return path

def print_and_assert(val, test):
    print(f'Got {val} expected {test}')
    assert val == test

def int_list_from_file(input_file, sep=","):
    with open(input_file, 'r') as infile:
        ll = [int(a) for a in infile.readline().split(sep)]

    return ll

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' % (method.__name__, (te - ts) * 1000))
        return result
    return timed
