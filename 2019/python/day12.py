import pytest
from aoc import save_test_file
from aoc import print_and_assert

from dataclasses import dataclass
from dataclasses import field

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import re


@dataclass
class Coord:
    x: int
    y: int
    z: int

    @staticmethod
    def starting_velocity():
        return Coord(0,0,0)

    def signed_diff(self, other):
        return Coord.sign(other.x - self.x), Coord.sign(other.y - self.y), Coord.sign(other.z - self.z)

    def change_with(self, dx, dy, dz):
        self.x = self.x + dx
        self.y = self.y + dy
        self.z = self.z + dz

    def apply(self, other):
        self.x = self.x + other.x
        self.y = self.y + other.y
        self.z = self.z + other.z

    @staticmethod
    def sign(i):
        return 1 if i > 0 else -1 if i < 0 else 0

    def abs_sum(self):
        return abs(self.x) + abs(self.y) + abs(self.z)

    def abs(self):
        return Coord(abs(self.x), abs(self.y), abs(self.z))

    def __add__(a, b):
        return Coord(a.x + b.x, a.y + b.y, a.z + b.z)

    def tolist(self):
        return [self.x, self.y, self.z]

@dataclass
class Moon:
    position: Coord
    velocity: Coord = field(default=Coord.starting_velocity())

    @staticmethod
    def from_pos(x,y,z):
        return Moon(Coord(x,y,z), Coord(0,0,0))


    def apply_gravity_from(self, other_moon):
        d_velocity = self.position.signed_diff(other_moon.position)
        self.velocity.change_with(*d_velocity)

    def update_position(self):
        self.position.apply(self.velocity)

    def kinetic_energy(self):
        return self.velocity.abs()

    def potential_energy(self):
        return self.position.abs()

    def total_energy(self):
        return self.kinetic_energy() + self.potential_energy()


def parse_moons_from(input_file):

    moons = []
    regex = r'<x=(.+), y=(.+), z=(.+)>'
    with open(input_file) as f:

        for line in f.read().splitlines():
            (x,y,z) = re.match(regex, line).groups()
            (x,y,z) = map( lambda x: int(x), (x,y,z))
            moons.append(Moon.from_pos(x,y,z))

    return moons


def step(moons):
    for m1 in moons:
        for m2 in moons:
            m1.apply_gravity_from(m2)

    kinetic_energies = []
    potential_energies = []
    for m1 in moons:
        m1.update_position()
        kinetic_energies.append(m1.kinetic_energy())
        potential_energies.append(m1.potential_energy())

    return np.sum(kinetic_energies), np.sum(potential_energies)


def run_system_for(num_steps, moons):
    kinetic_energies = []
    potential_energies = []
    for _ in range(num_steps):
        ke, pe = step(moons)
        kinetic_energies.append(ke)
        potential_energies.append(pe)

    return kinetic_energies, potential_energies


def calculate_total_energy(moons):
    total_energy = Coord(0,0,0)
    for moon in moons:
        total_energy = total_energy + moon.total_energy()

    return total_energy 

def setup_and_run(input_file, num_steps):
    moons = parse_moons_from(input_file)
    run_system_for(num_steps, moons)
    return calculate_total_energy(moons)


def part_1(input_file, num_steps = 1000):
    print(setup_and_run(input_file, num_steps))

def find_first_zero(dfke, coord):

    zeros = dfke[dfke[coord] == 0]

    if len(zeros) == 0:
        raise ValueError(f'Not enough iterations for {coord}, no zeros')
    else:
        return zeros.head(1).index.values[0] + 1

def part_2(input_file, num_steps):
        moons = parse_moons_from(input_file)
        ke, pe = run_system_for(num_steps, moons)

        dfke = pd.DataFrame( list(map(lambda x: x.tolist(), ke)), columns=["x","y","z"])
        x0 = find_first_zero(dfke, 'x')
        y0 = find_first_zero(dfke, 'y')
        z0 = find_first_zero(dfke, 'z')

        print(x0*y0*z0)

class TestCases(object):

    def test_coord(self):
        assert Coord(1,1,1).signed_diff(Coord(0, 2, 1)) == (-1, 1, 0)


    def test_moon(self):
        m1 = Moon.from_pos(1,1,1)
        m1.apply_gravity_from(Moon.from_pos(0,2,1)) 
        assert m1.velocity == Coord(-1,1,0)


    def test_two_moons(self):
        m1 = Moon.from_pos(1,1,1)
        m2 = Moon.from_pos(-1,1,0)
        moons = [ m1, m2]
        step(moons)

        assert m1 == Moon(Coord(0,1,0), Coord(-1, 0, -1))
        assert m2 == Moon(Coord(0,1,1), Coord(1, 0, 1))


    def test_reading_input(self):
        input_string = """<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>"""
        moons = parse_moons_from(save_test_file(input_string))
        print_and_assert(moons, [Moon.from_pos(-1, 0, 2), Moon.from_pos(2,-10, -7), Moon.from_pos(4, -8, 8), Moon.from_pos(3,5,-1)])
        
        ke, pe = run_system_for(2772, moons)

        dfke = pd.DataFrame( list(map(lambda x: x.tolist(), ke)), columns=["x","y","z"])

        x0 = find_first_zero(dfke, 'x')
        y0 = find_first_zero(dfke, 'y')
        z0 = find_first_zero(dfke, 'z')

        assert x0 * y0 * z0 == 2772

        
    def test_part1(self):
        part_1('../inputs/day12.txt')


    def test_part2(self):
        part_2('../inputs/day12.txt', 1000000)


