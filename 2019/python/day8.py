import pytest
from aoc import save_test_file
from aoc import int_list_from_file
from aoc import print_and_assert

import operator

from collections import Counter
import numpy as np
import matplotlib.pyplot as plt

def part_1(input_file, width, height):

    input_file = '../inputs/day_8.txt'
    width = 25
    height = 6

    all_pixels = str(int_list_from_file(input_file)[0])
    num_pixels = len(all_pixels)

    layer_size = width * height

    number_of_layers = int(num_pixels/layer_size)
    assert number_of_layers * layer_size == num_pixels

    layers = []
    for layer in range(number_of_layers):
        layers.append(all_pixels[layer*layer_size:((layer + 1)*layer_size)])

    zots = [(a.count('0'), a.count('1'), a.count('2')) for a in layers]

    zots.sort(key=lambda x: x[0])

    most_zeros = zots[0]
    print(most_zeros[1]*most_zeros[2])


def part_2(input_file, width, height):

    input_file = '../inputs/day_8.txt'
    width = 25
    height = 6

    all_pixels = str(int_list_from_file(input_file)[0])
    num_pixels = len(all_pixels)

    layer_size = width * height

    number_of_layers = int(num_pixels/layer_size)
    assert number_of_layers * layer_size == num_pixels

    cumulative_layer = 2 * np.ones((25*6, ), dtype=int)
    for layer in range(number_of_layers):
        pixel_string = all_pixels[layer*layer_size:((layer + 1)*layer_size)]

        next_layer = np.array(list(pixel_string), dtype=int)
        index_of_twos = np.where(cumulative_layer == 2)
        cumulative_layer[index_of_twos] = next_layer[index_of_twos]

    final = np.reshape(cumulative_layer, (height, width))
    

    plt.imshow(final)


