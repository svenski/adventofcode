import pytest

import pandas as pd



def part_1(input_file):

    masses = read_from(input_file)
    fuel_required = calculate_fuel_req(masses)

    return fuel_required

def part_2(input_file):
    masses = read_from(input_file)
    fuel_required = calculate_fuel_req_part_2(masses)

    return fuel_required

def calculate_fuel_req(masses):
    return  ((masses / 3).astype(int) - 2).sum()

def calc_fuel(mass):
    return int(mass/3) - 2

def calculate_fuel_req_part_2(masses):

    total_sum = 0

    for mm in masses:
        mm_sum = 0
        this_mm = mm
        while this_mm > 0:
            this_mm = calc_fuel(this_mm)
            if this_mm < 0: this_mm = 0
            mm_sum = mm_sum + this_mm

        total_sum = total_sum + mm_sum


    return total_sum


def read_from(input_file):
    return pd.read_csv(input_file, header=None, names=['data'], dtype=int, sep="\t")['data']

class TestPart1(object):

    def test_example(self):
        input_string = """
100756 
1969"""
        test_file = "./input/day_test.txt"
        with open(test_file, 'w') as output:
            output.write(input_string)
        out = part_1(test_file)

        assert out == (33583 + 654)

    def part_test(self):

        print(part_1('./input/day1_input.txt'))


    def test_part2():

        calculate_fuel_req_part_2([100756]) 
        calculate_fuel_req_part_2([1969])


