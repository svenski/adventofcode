import pytest
from aoc import save_test_file
from aoc import print_and_assert

import operator
import cmath

from itertools import groupby
from operator import itemgetter


def part_1(input_file):
    astroids = parse_astroids_from(input_file)
    return find_astroid_with_best_view(astroids)[1]

def normalise_phase(phi, offset=0):
    return (phi + (2 + offset)*cmath.pi) % (2 * cmath.pi)


def part_2(input_file, to_take = 5):
    # input_file = '../inputs/day_10_test.txt'
    astroids = parse_astroids_from(input_file)
    centre_astroid = find_astroid_with_best_view(astroids)[0]
    
    path_to_others = [x - centre_astroid for x in astroids if x != centre_astroid]

    others_polar = [cmath.polar(x) for x in path_to_others]
    others_polar = [(r,normalise_phase(phi)) for r, phi in others_polar]

    # Hacky solution to get the boundary right 
    others_polar = sorted(others_polar, key=lambda x: (-1*normalise_phase(x[1], -0.5000001), x[0]))
    angle_groups = groupby(others_polar, itemgetter(1))

    astroids_grouped_by_angle =  [[phase for phase in coords] for (phase, coords) in angle_groups]

    number_of_groups = len(astroids_grouped_by_angle)
    taken = 0
    current_group = 0

    while taken < to_take:
        current_group = current_group % number_of_groups
        if(len(astroids_grouped_by_angle[current_group]) > 0):
            next_astroid = astroids_grouped_by_angle[current_group].pop(0)
            taken = taken + 1
        current_group = current_group + 1

    coord_to_nth = cmath.rect(*next_astroid) + centre_astroid

    print(coord_to_nth)
    return 100*round(coord_to_nth.real)+ round(abs(coord_to_nth.imag))



def find_astroid_with_best_view(astroids):
    astroids_seen = []
    
    for astroid in astroids:
        unique_angles = set([cmath.polar(x - astroid)[1] for x in astroids if x != astroid])
        astroids_seen.append((astroid, len(unique_angles)))

    astroids_seen.sort(key=itemgetter(1), reverse=True)
    return astroids_seen[0]


def parse_astroids_from(input_file):
    astroids = []
    with open(input_file) as f:
        row_num = 0
        for row in f.read().splitlines():
            astroids.extend([i + row_num*1j for i, item in enumerate(row) if item == '#'])
            row_num = row_num - 1

    return astroids




def test_example():
    input_string = """......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####"""

    
    print_and_assert(part_1(save_test_file(input_string)), 33)


    input_string = """.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##
"""

    print_and_assert(part_1(save_test_file(input_string)), 210)
    print_and_assert(part_2(save_test_file(input_string), 10), 1208)

def part_part1(self):
    input_file = '../inputs/day_10.txt'
    print(part_1(input_file))



def test_part2():
    input_file = '../inputs/day_10.txt'
    print(part_2(input_file, 200))


test_example()

