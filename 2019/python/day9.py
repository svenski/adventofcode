import pytest

import operator

from aoc import save_test_file
from aoc import print_and_assert

import logging
logging.basicConfig(level=logging.DEBUG)

from collections import deque
from itertools import permutations

from dataclasses import dataclass
from dataclasses import field
from typing import List


@dataclass
class IntCode:
    program: List[int]
    inputs: List[int] = field(default_factory=list)
    current_position: int = field(default=0)
    terminated: bool = field(default=False)
    relative_base:int = field(default=0)


    def __repr__(self):
        return f'IntCode({self.current_position}, {self.terminated}'
        # return f'IntCode({self.current_position=}, {self.terminated=}'


    def advance(self, steps):
        self.current_position = self.current_position + steps
        diff = len(self.program) - self.current_position
        if diff < 0:
            logging.debug(f'Extending program with {diff} memory')
            self.program = self.program + ([0] * abs(diff))

    def set_program_index(self, index, value):
        if index >= len(self.program):
            self.program = self.program + ([0] * (index-len(self.program) + 1))
        self.program[index] = value

    def get_program_index(self, index):
        if index >= len(self.program):
            return 0
        else:
            return self.program[index]

    def run_program(self, inputs):
        self.inputs.append(inputs)
        opcode, param1_mode, param2_mode, param3_mode = IntCode.parse_from(self.get_program_index(self.current_position))
        logging.debug(f'{ opcode}, { param1_mode}, { param2_mode}, { param3_mode} ')

        input1 =None
        outputs = []
        i = 0
        while opcode != 99:
            if opcode == 1:
                self.binary_operation_for(operator.add, param1_mode, param2_mode, param3_mode)
            elif opcode == 2:
                self.binary_operation_for(operator.mul, param1_mode, param2_mode, param3_mode)
            elif opcode == 3:
                next_input = self.inputs.pop(0)
                output_pos = self.param_for(1, param1_mode, True)
                logging.debug(f'Using input: {next_input} for position: {output_pos}')
                self.set_program_index(output_pos, next_input)
                self.advance(2)
            elif opcode == 4:
                input1 = self.param_for(1, param1_mode)
                logging.debug(f'Got output: { input1 }')
                outputs.append(input1)
                self.advance(2)
            elif opcode == 5:
                self.unary_test_for(operator.ne, param1_mode, param2_mode)
            elif opcode == 6:
                self.unary_test_for(operator.eq, param1_mode, param2_mode)
            elif opcode == 7:
                self.binary_comparison_for(operator.lt, param1_mode, param2_mode, param3_mode)
            elif opcode == 8:
                self.binary_comparison_for(operator.eq, param1_mode, param2_mode, param3_mode)
            elif opcode ==9:
                change_to_rb = self.param_for(1, param1_mode)
                self.relative_base = self.relative_base + change_to_rb
                self.advance(2)
            else:
                raise ValueError(f'The opcode is not a valid value: {opcode}')

            logging.debug(f'Next program instruction: {self.program[self.current_position]} at {self.current_position}')
            # logging.debug(f'Next program instruction: {self.program[self.current_position]} at {self.current_position=}')
            opcode, param1_mode, param2_mode, param3_mode = IntCode.parse_from(self.get_program_index(self.current_position))
            logging.debug(f'{ opcode}, { param1_mode}, { param2_mode}, { param3_mode} , {self.current_position}, {self.relative_base}')
            # logging.debug(f'{ opcode= }, { param1_mode= }, { param2_mode= }, { param3_mode= } , {self.current_position=}, {self.relative_base=}')
            # logging.debug(f'{self.program}')

            # i+=1
            # if i == 10:
                # break

        if opcode == 99:
            self.terminated = True
        return outputs


    def param_for(self, offset, param_mode, output=False):
        instr = self.get_program_index(self.current_position + offset)

        if output:
            if param_mode == 2:
                return instr + self.relative_base
            else:
                return instr

                #return self.get_program_index(instr)
        else:
            if param_mode == 0:
                return self.get_program_index(instr)
            elif param_mode == 1:
                return instr
            elif param_mode == 2:
                return self.get_program_index(instr + self.relative_base)
            else:
                raise ValueError(f'param mode: {param_mode} is not supported')


    def binary_operation_for(self, operand, param1_mode, param2_mode, param3_mode):
        self.set_program_index(self.param_for(3, param3_mode, True), operand(self.param_for(1, param1_mode), self.param_for(2, param2_mode)))

        self.advance(4)


    def unary_test_for(self, operand, param1_mode, param2_mode):
        if operand(self.param_for(1, param1_mode), 0):
            self.advance(self.param_for(2, param2_mode) - self.current_position)
        else:
            self.advance(3)


    def binary_comparison_for(self, operand, param1_mode, param2_mode, param3_mode):

        output_pos = self.param_for(3, param3_mode, True)

        if operand(self.param_for(1, param1_mode), self.param_for(2, param2_mode)):
            out_val = 1
        else:
            out_val = 0

        self.set_program_index(output_pos, out_val)
        self.advance(4)


    def param_for_Y(self, offset, param_mode):
        instr = self.get_program_index(self.current_position + offset)
        return instr if param_mode == 1 else self.get_program_index(instr)




    @staticmethod
    def parse_from(op_instr):
        op_str = f'{op_instr:05}'
        logging.debug(op_str)
        opcode = int(op_str[-2:])
        param1_mode = int(op_str[2])
        param2_mode = int(op_str[1])
        param3_mode = int(op_str[0])
        return opcode, param1_mode, param2_mode, param3_mode

    

def get_output_for(program, inputs):
    aa = IntCode(program[:])
    out = aa.run_program(inputs)
    return out



def list_from_file(inputfile):
    with open(inputfile, 'r') as infile:
        ll = [int(a) for a in infile.readline().split(',')]

    return ll


def test_example():

    
    program_str ="109, -1, 4, 1, 99"
    output = get_output_for( list_from_file(save_test_file(program_str)), [])[0]
    print_and_assert(get_output_for([109, -1, 104, 1, 99], [])[0], 1)
    print_and_assert(get_output_for([109, -1, 204, 1, 99], [])[0], 109)
    print_and_assert(get_output_for([109, 1, 9, 2, 204, -6, 99] , [])[0], 204)
    print_and_assert(get_output_for([109, 1, 209, -1, 204, -106, 99] , [])[0], 204)
    print_and_assert(get_output_for([109, 1, 3, 3, 204, 2, 99], [333])[0], [333])
    print_and_assert(get_output_for([109, 1, 203, 2, 204, 2, 99], [33])[0], [33])


    # Day 5 tests
    print_and_assert(get_output_for(list_from_file(save_test_file("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9")), 0), [0])
    print_and_assert(get_output_for(list_from_file(save_test_file("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9")), 3), [1])

    program = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31, 1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104, 999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99"
    print_and_assert(get_output_for(list_from_file(save_test_file(program)), 3), [999])
    print_and_assert(get_output_for(list_from_file(save_test_file(program)), 8), [1000])
    print_and_assert(get_output_for(list_from_file(save_test_file(program)), 10), [1001])

    ##### Day 9 tests
    program_str = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"
    print_and_assert(get_output_for( list_from_file(save_test_file(program_str)), []), [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99])

    program_str = "1102,34915192,34915192,7,4,7,99,0"
    output = get_output_for( list_from_file(save_test_file(program_str)), [])[0]
    print_and_assert(len(str(output)), 16)

    program_str = "104,1125899906842624,99"
    output = get_output_for( list_from_file(save_test_file(program_str)), [])
    print_and_assert(output,[1125899906842624])



def part1():
    inputfile = '../inputs/day_9.txt'
    get_output_for(list_from_file(inputfile),[1])


def main_part2():
    inputfile = '../inputs/day_9.txt'
    part_2(inputfile)



test_example()
