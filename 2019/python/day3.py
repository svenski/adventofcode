import pytest
from aoc import save_test_file

import operator

from enum import Enum

from dataclasses import dataclass
from typing import List

from cmath import polar

def part_1(input_file):
    wires =  parse_wires_from_file(input_file)

    wire_coords = []

    for wire in wires:
        wire_coords.append(wire.calculate_full_path())

    
    crossings = set(wire_coords[0]).intersection(set(wire_coords[1]))

    # crossings_sorted  = sorted(crossings, key= lambda x: abs(x.real) + abs(x.imag))
    manhattan_distance = [abs(x.real) + abs(x.imag) for x in crossings]
    manhattan_distance.sort()
    return manhattan_distance[0]
    

def part_2(input_file):
    wires =  parse_wires_from_file(input_file)

    wire_coords = []

    for wire in wires:
        wire_coords.append(wire.calculate_full_path())
    
    crossings = set(wire_coords[0]).intersection(set(wire_coords[1]))

    steps_to_crossings = []

    for crossing in crossings:
        total = wire_coords[0].index(crossing) + wire_coords[1].index(crossing) + 2
        steps_to_crossings.append(total)

    steps_to_crossings.sort()
    return steps_to_crossings[0]


def parse_wires_from_file(input_file):

    wires = []
    with open(input_file, 'r') as infile:
        for line in infile.readlines():
            wires.append(parse_wire_from_line(line))

    return wires


def parse_wire_from_line(line):
    raw_steps = line.split(',')
    steps=[]
    for rs in raw_steps:
        steps.append(to_complex(rs))

    return Wire(steps)


def to_complex(rs):
    d = Dir[rs[0]]
    r = int(rs[1:])
    return d.value*r


class Dir(Enum):
    R = 1
    L = -1
    U = 1j
    D = -1j


@dataclass
class Wire():
    coords: List[complex]

    def calculate_full_path(self):

        path_coords = []
        current_coord = 0

        for step_coord in self.coords:
            stop = current_coord + step_coord
            step = step_coord / polar(step_coord)[0]

            while (next_coord := current_coord + step):
                path_coords.append(next_coord)
                current_coord = next_coord
                if next_coord == stop: 
                    break

        return path_coords

def print_and_assert(val, test):
    print(f'Got {val}')
    assert val == test

class TestPart1(object):

    def test_example(self):
        input_string = """XX"""

        print_and_assert(part_1( save_test_file("R8,U5,L5,D3\nU7,R6,D4,L4", 3)), 6)
        print_and_assert(part_1( save_test_file("R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83", 3)), 159)
        print_and_assert(part_1( save_test_file("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 3)), 135)


    def part_part(self):
        input_file = '../inputs/day_3.txt'
        part_1(input_file)


    def test_example(self):
        print_and_assert(part_2( save_test_file("R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83", 3)), 610)
        print_and_assert(part_2( save_test_file("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 3)), 410)

    def test_part2():
        input_file = '../inputs/day_3.txt'
        part_2(input_file)


