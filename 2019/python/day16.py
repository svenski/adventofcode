import pytest
from aoc import save_test_file
from aoc import print_and_assert
from aoc import int_list_from_file
from aoc import timeit

import numpy as np

def part_1(input_file, num_phases):

    signal, _ = parse_input(input_file)
    signal = np.array(parse_input(input_file))
    signal_length = len(signal)

    for i in range(num_phases):
        if i % 20 == 0:
            print(f'Current phase = {i}')
        next_signal = []
        for cycle in range(signal_length):
            pp = np_pattern_for(signal_length, cycle)
            out = (np.multiply(signal, pp)).sum()
            next_signal.append(abs(out) % 10)
        signal = np.array(next_signal)


    return "".join(map(str, signal))

@timeit
def pattern_for(signal_length):
    
    patterns = np.zeros((signal_length, signal_length + 1), dtype=np.int8)
    base_battern = np.array([0,1,0,-1])
    for cycle in range(signal_length):
        num_reps = cycle + 1
        cycle_pattern = np.repeat(base_battern, num_reps)
        if len(cycle_pattern) > signal_length:
            patterns[cycle, :] = cycle_pattern[:(signal_length + 1)]
        else:
            num_tiles = (signal_length + 1) // len(cycle_pattern)
            odd_bits = (signal_length + 1) % len(cycle_pattern)
            pp = np.tile(cycle_pattern, num_tiles)
            pp = np.append(pp, cycle_pattern[:odd_bits])
            patterns[cycle, :] = pp

    return patterns[:, 1:]


def np_pattern_for(signal_length, cycle):
    num_reps = cycle + 1
    base_battern = np.array([0,1,0,-1])
    cycle_pattern = np.repeat(base_battern, num_reps)
    if len(cycle_pattern) > signal_length:
        return cycle_pattern[:(signal_length + 1)][1:]
    else:
        num_tiles = (signal_length + 1) // len(cycle_pattern)
        odd_bits = (signal_length + 1) % len(cycle_pattern)
        pp = np.tile(cycle_pattern, num_tiles)
        pp = np.append(pp, cycle_pattern[:odd_bits])
        return pp[1:]

def part_2(input_file, num_phases):
    signal = np.tile(np.array(parse_input(input_file)), 10000)
    offset = int("".join(map(str, signal[:7].tolist())))
    
    signal_length = len(signal)

    for i in range(num_phases):
        print(f'Current phase = {i}')
        next_signal = []
        for cycle in range(signal_length):
            pp = np_pattern_for(signal_length, cycle)
            out = (np.multiply(signal, pp)).sum()
            next_signal.append(abs(out) % 10)
        signal = np.array(next_signal)


    output = "".join(map(str, signal))
    return output[offset:(offset+8)]


def parse_input(input_file):
    with open(input_file, 'r') as infile:
        input_str = list(infile.readline().strip())
        ints = [int(a) for a in input_str]

    offset = int(input_str[:7])

    return ints, offset


class TestCases(object):

    def test_examples(self):
        string_in = "12345678"
        input_file =  save_test_file(string_in)
        print_and_assert( part_1(input_file, 4), "01029498")


        string_in = "80871224585914546619083218645595"
        input_file =  save_test_file(string_in)
        print_and_assert( part_1(input_file, 100)[:8], "24176176")

        string_in = "19617804207202209144916044189917"
        input_file =  save_test_file(string_in)
        print_and_assert( part_1(input_file, 100)[:8], "73745418")


        string_in = "69317163492948606335995924319873"
        input_file =  save_test_file(string_in)
        print_and_assert( part_1(input_file, 100)[:8], "52432133")

    def test_examples2(self):
        string_in = "03036732577212944063491565474664"
        input_file =  save_test_file(string_in)
        print_and_assert( part_2(input_file, 100), "84462026")


    def test_part1(self):
        input_file = '../inputs/day16.txt'
        part_1(input_file, 100)

    def test_part2(self):
        None


