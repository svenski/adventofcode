import pytest
from aoc import save_test_file
from aoc import print_and_assert

from typing import List

from dataclasses import dataclass
from dataclasses import field

from collections import deque

import re


DAY = 22

@dataclass
class Deck:
    size: int
    cards: List = field(init=False)

    def __post_init__(self):
        self.cards = list(range(self.size))

    def deal_into_new_stack(self):
        self.cards.reverse()

    def cut(self, n):
        dd = deque(self.cards)
        dd.rotate(-1*n)
        self.cards = list(dd)


    def deal_with_increment(self, n):
        cc = self.cards[:]
        table = [None] * self.size

        current_pos = 0
        while len(cc) > 0:
            table[current_pos] = cc.pop(0)
            current_pos = (current_pos + n) % self.size

        #assert -1 not in table
        self.cards = table

def part_1(input_file, size):

    deck = Deck(size)

    with open(input_file) as infile:
        for line in infile.read().splitlines():
            if line == "deal into new stack":
                deck.deal_into_new_stack()
            elif (match := re.match(r"cut (.+)$", line)) is not None:
                cut_size = int(match.group(1))
                deck.cut(cut_size)
            elif (match := re.match(r"deal with increment (\d+)$", line)) is not None:
                inc = int(match.group(1))
                deck.deal_with_increment(inc)
            else:
                raise ValueError('Cannot parse {line=}')

    return deck

def part_1_single_follow_card(input_file, size, card):

    current_position = card
    with open(input_file) as infile:
        for line in infile.read().splitlines():
            if line == "deal into new stack":
                current_position = size - current_position - 1
            elif (match := re.match(r"cut (.+)$", line)) is not None:
                cut_size = int(match.group(1))
                current_position = (current_position - cut_size) % size
            elif (match := re.match(r"deal with increment (\d+)$", line)) is not None:
                inc = int(match.group(1))
                current_position = (current_position * inc) % size
            else:
                raise ValueError('Cannot parse {line=}')

    return current_position

def part_2(input_file):
    None


def intlist_from(digs):
    return list(map(int,digs.split(" ")))

class TestCasesPart2(object):

    def test_cut_follow_one(self):

        shuffle="""cut 3"""
        size = 7

        print_and_assert(part_1_single_follow_card( save_test_file(shuffle), size, 2), 6)
        print_and_assert(part_1_single_follow_card( save_test_file(shuffle), size, 4), 1)

        shuffle="""cut -3"""
        print_and_assert(part_1_single_follow_card( save_test_file(shuffle), size, 2), 5)
        print_and_assert(part_1_single_follow_card( save_test_file(shuffle), size, 4), 0)


    def test_deal_into_new_stack_one(self):
        shuffle = """deal into new stack"""
        size = 7

        print_and_assert(part_1_single_follow_card( save_test_file(shuffle), size, 0), 6)
        print_and_assert(part_1_single_follow_card( save_test_file(shuffle), size, 6), 0)

    
    def test_deal_with_increment_one(self):
        shuffle = """deal with increment 3"""
        size = 10

        print_and_assert(part_1_single_follow_card( save_test_file(shuffle), size, 7), 1)
        print_and_assert(part_1_single_follow_card( save_test_file(shuffle), size, 3), 9)


        
    def test_part1(self):
        input_file = f'../inputs/day{DAY}.txt'
        print_and_assert(part_1_single_follow_card(input_file, 10007, 2019), 1822)



class TestCases(object):

    @staticmethod
    def verify_shuffle_output(size, shuffles, output_order):
        print_and_assert( part_1( save_test_file(shuffles), size).cards, 
                intlist_from(output_order))

    def test_deal_into_new_stack(self):
        d = Deck(size=10)
        d.deal_into_new_stack()
        assert d.cards == list(reversed(range(10)))


    def test_cut(self):
        d = Deck(10)
        d.cut(3)
        assert d.cards == intlist_from("3 4 5 6 7 8 9 0 1 2")
        
        d = Deck(10)
        d.cut(-4)
        assert d.cards == intlist_from("6 7 8 9 0 1 2 3 4 5")
    
    def test_deal_with_increment(self):
        d = Deck(10)
        d.deal_with_increment(3)
        print_and_assert(d.cards, intlist_from("0 7 4 1 8 5 2 9 6 3"))


    def test_examples(self):
        size = 10

        shuffles = """deal with increment 7
deal into new stack
deal into new stack"""

        TestCases.verify_shuffle_output(size, shuffles, "0 3 6 9 2 5 8 1 4 7")

        shuffles = """cut 6
deal with increment 7
deal into new stack"""
        TestCases.verify_shuffle_output(size, shuffles, "3 0 7 4 1 8 5 2 9 6")

        shuffles = """deal into new stack
cut -2
deal with increment 7
cut 8
cut -4
deal with increment 7
cut 3
deal with increment 9
deal with increment 3
cut -1"""
        TestCases.verify_shuffle_output(size, shuffles, "9 2 5 8 1 4 7 0 3 6")

    
    def test_part1(self):
        input_file = f'../inputs/day{DAY}.txt'
        shuffled_deck = part_1(input_file, 10007)
        print(shuffled_deck.cards.index(2019))



    def test_part2(self):
        input_file = f'../day{DAY}.txt'
        # shuffled_deck = part_1(input_file, 119315717514047)
        # print(shuffled_deck.cards.index(2019))


