import pytest
from aoc import save_test_file
import functools

import operator

def part_1(pz_in):
    return run_for(pz_in, check_conditions_passes_part1)

def part_2(input_file):
    return run_for(input_file, check_conditions_passes_part2)

def run_for(pz_in, fun):
    start,end = map(lambda x: int(x), pz_in.split('-'))

    number_of_candidates = 0

    for pass_cand in range(start, end):

        if fun(pass_cand):
            number_of_candidates = number_of_candidates + 1

    return number_of_candidates


class TestPart1(object):

    pz_in = "197487-673251"
    def test_example(self):
        assert check_conditions_passes_part1(111111) == True
        assert check_conditions_passes_part1(223450) == False
        assert check_conditions_passes_part1(123789) == False

        print(part_1(pz_in))

    def part_part(self):
        assert check_conditions_passes_part2(123444) == False
        assert check_conditions_passes_part2(111122) == True


    def test_part2():
        print(part_2(pz_in))



def check_conditions_passes_part1(pass_cand):
    seq = str(pass_cand)
    return increasing(seq) and has_similar_neighbour(seq)


def check_conditions_passes_part2(pass_cand):
    seq = str(pass_cand)

    passes = False
    if increasing(seq):
        same_lag1 = [earlier == later for earlier, later in zip(seq, 'a' + seq[:-1])]
        same_lag1_lag2 = [earlier == later == latest for earlier, later, latest in zip(seq, 'a' + seq[:-1], 'bb' + seq[:-2])]
        same_lag_1_to_remove = [x or y for x,y in zip(same_lag1_lag2, same_lag1_lag2[1:] + [False])]

        final = []
        for i, x in enumerate(same_lag1):
            if same_lag_1_to_remove[i]:
                final.append(False)
            else:
                final.append(x)
            
        passes = any(final)

    return passes

    

def increasing(seq):
    return all(earlier <= later for earlier, later in zip(seq, seq[1:]))

def has_similar_neighbour(seq):
    return any(earlier == later for earlier, later in zip(seq, seq[1:]))


