import pytest
from aoc import save_test_file
from aoc import print_and_assert

DAY = XX

def part_1(input_file):
    None


def part_2(input_file):
    None


class TestCases(object):

    def test_examples(self):
        print_and_assert( part_1( save_test_file("XX")), "bla")

    def test_part1(self):
        input_file = f'../inputs/day{DAY}.txt'
        part_1(input_file)


    def test_part2(self):
        input_file = f'../inputs/day{DAY}.txt'
        part_1(input_file)


