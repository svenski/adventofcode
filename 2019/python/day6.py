import pytest
from aoc import save_test_file
from aoc import print_and_assert

import operator

from dataclasses import dataclass
from collections import defaultdict

def part_1(input_file):
    orbits, _ = parse_to_dict(input_file)

    orb_counts = 0
    stack = [('COM', 0)]
    
    while len(stack) > 0:
        orig, depth = stack.pop()
        for orber in orbits[orig]:
            stack.append((orber, depth + 1))

        orb_counts = orb_counts + depth

    return orb_counts


def part_2(input_file):
    orbits, orber_to_parents = parse_to_dict(input_file)
    
    ofi = ['YOU', 'SAN']

    # find common orbiting planet
    common = None
    current_parent = {'YOU': 'YOU', 'SAN': 'SAN'}
    all_parents = set()
    
    while common is None:
        for this_obj in ofi:
            next_parent = orber_to_parents[current_parent[this_obj]]

            if next_parent in all_parents:
                common = next_parent
                break

            all_parents.add(next_parent)
            current_parent[this_obj] = next_parent


    orb_counts = 0
    stack = [(common, 0)]
    
    total_steps = 0
    while len(stack) > 0:
        orig, depth = stack.pop()
        for orber in orbits[orig]:
            if orber in ofi:
                total_steps = total_steps + depth
            stack.append((orber, depth + 1))

        orb_counts = orb_counts + depth
    
    return total_steps



def parse_to_dict(input_file):

    orbs = defaultdict(list)
    orber_to_parent = {}
    with open(input_file, 'r') as orbfile:
        for line in orbfile:
            line = line.strip()
            (orig, orber) = line.split(')')
            orbs[orig].append(orber)
            orber_to_parent[orber] = orig


    return orbs, orber_to_parent



class TestPart1(object):

    def test_example(self):
        input_string = """COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
        """

        print_and_assert(part_1(save_test_file(input_string, 6)), 42)

        input_string ="""COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN
"""

        print_and_assert(part_2(save_test_file(input_string, 6)), 4)


    def part_part1(self):
        print(part_1('../inputs/day6_input.txt'))


    def test_part2():
        print(part_2('../inputs/day6_input.txt'))


