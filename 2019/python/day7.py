import pytest

import operator

from aoc import save_test_file
from aoc import print_and_assert

import logging
#logging.basicConfig(level=logging.DEBUG)

from collections import deque
from itertools import permutations

from dataclasses import dataclass
from dataclasses import field
from typing import List


@dataclass
class Amplifier:
    program: List[int]
    phase: int
    inputs: List[int] = field(default_factory=list)
    current_position: int = field(default=0)
    terminated: bool = field(default=False)

    def __post_init__(self):
        self.inputs.append(self.phase)

    def __repr__(self):
        return f'Amplifier({self.phase=}, {self.current_position=}, {self.terminated=}'


    def advance(self, steps):
        self.current_position = self.current_position + steps

    def run_program(self, inputs):
        self.inputs.append(inputs)
        opcode, param1_mode, param2_mode = parse_from(self.program[self.current_position])

        input1 =None
        send_output = False
        while opcode != 99 and not send_output:
            if opcode == 1:
                self.binary_operation_for(operator.add, param1_mode, param2_mode)
            elif opcode == 2:
                self.binary_operation_for(operator.mul, param1_mode, param2_mode)
            elif opcode == 3:
                next_input = self.inputs.pop(0)
                output_pos = self.program[self.current_position + 1]
                self.program[output_pos] = next_input
                self.advance(2)
            elif opcode == 4:
                input1 = self.param_for(1, param1_mode)
                self.advance(2)
                send_output = True
            elif opcode == 5:
                self.unary_test_for(operator.ne, param1_mode, param2_mode)
            elif opcode == 6:
                self.unary_test_for(operator.eq, param1_mode, param2_mode)
            elif opcode == 7:
                self.binary_comparison_for(operator.lt, param1_mode, param2_mode)
            elif opcode == 8:
                self.binary_comparison_for(operator.eq, param1_mode, param2_mode)
            else:
                raise ValueError(f'The opcode is not a valid value: {opcode}')

            opcode, param1_mode, param2_mode = parse_from(self.program[self.current_position])

        if opcode == 99:
            self.terminated = True
        return input1


    def binary_operation_for(self, operand, param1_mode, param2_mode):
        self.program[self.param_for(3, 1)] = operand(self.param_for(1, param1_mode), self.param_for(2, param2_mode))
        self.advance(4)


    def unary_test_for(self, operand, param1_mode, param2_mode):
        if operand(self.param_for(1, param1_mode), 0):
            self.current_position = self.param_for(2, param2_mode)
        else:
            self.advance(3)

    def binary_comparison_for(self, operand, param1_mode, param2_mode):

        output_pos = self.param_for(3, 1)

        if operand(self.param_for(1, param1_mode), self.param_for(2, param2_mode)):
            self.program[output_pos] = 1
        else:
            self.program[output_pos] = 0

        self.advance(4)


    def param_for(self, offset, param_mode):
        instr = self.program[self.current_position + offset]
        return instr if param_mode == 1 else self.program[instr]


def part_1(inputfile):
    return find_max_for(inputfile, range(0,5), get_output_for_phases)


def part_2(inputfile):
    return find_max_for(inputfile, range(5,10), get_output_for_phases_with_feedback)


def find_max_for(inputfile, phase_range, func):

    program = list_from_file(inputfile)

    number_of_amps = 5
    phase_settings = list(permutations(list(phase_range), number_of_amps))
    
    max_output = 0
    max_phases = -1


    for phases in phase_settings:
        output = func(program, phases)
        if output > max_output:
            max_output = output
            max_phases = phases

    return (max_output, max_phases)
    

def get_output_for_phases(program, phases, number_of_amps=5):
    inputs = 0
    amps = [Amplifier(program[:], phases[i]) for i in range(number_of_amps)]

    for amp_num in range(number_of_amps):
        inputs = amps[amp_num].run_program(inputs)

    return inputs

    
def get_output_for_phases_with_feedback(program, phases, number_of_amps=5):
    inputs = 0
    amps = [Amplifier(program[:], phases[i]) for i in range(number_of_amps)]

    previous_inputs = -1
    all_outputs = []
    while not all([amp.terminated for amp in amps]):
        for amp_num in range(number_of_amps):
            logging.debug(f'Passing {input=} to {amps[amp_num]}')
            inputs = amps[amp_num].run_program(inputs)
            all_outputs.append(inputs)

    outs = [oo for oo in all_outputs if oo is not None]
    return outs[-1]



def parse_from(op_instr):
    op_str = f'{op_instr:05}'
    opcode = int(op_str[-2:])
    param1_mode = int(op_str[2])
    param2_mode = int(op_str[1])
    return opcode, param1_mode, param2_mode


def list_from_file(inputfile):
    with open(inputfile, 'r') as infile:
        ll = [int(a) for a in infile.readline().split(',')]

    return ll


def test_example():
    program_str = "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"
    print_and_assert(get_output_for_phases( list_from_file(save_test_file(program_str, 7)), phases = [4,3,2,1,0]), 43210)
    print_and_assert(part_1(save_test_file(program_str, 7)), (43210, (4,3,2,1,0)))

    program_str = "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"
    print_and_assert(get_output_for_phases( list_from_file(save_test_file(program_str, 7)), phases = [0,1,2,3,4]), 54321)

    program_str = "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0"
    print_and_assert(get_output_for_phases( list_from_file(save_test_file(program_str, 7)), phases = [1,0,4,3,2]), 65210)

    program_str = "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5"
    print_and_assert(get_output_for_phases_with_feedback( list_from_file(save_test_file(program_str, 7)), phases = [9,8,7,6,5]), 139629729)

def part1():
    inputfile = '../inputs/day_9.txt'
    print(part_1(inputfile))


def main_part2():
    inputfile = '../inputs/day_9.txt'
    part_2(inputfile)


