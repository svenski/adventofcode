(ns aoc-day10
  (:require [clojure.string :refer [split-lines]]
            [clojure.test :refer [deftest is are]],
            [clojure.string :as str]))


( map-indexed vector (split-lines (slurp "../inputs/day_10_test.txt")))

  (apply list
  (take 1 (split-lines (slurp "../inputs/day_10_test.txt"))))

(read-string  (slurp "../inputs/day_10_test.txt"))


(deftest part1_test
  (is (part1) 3337604))


;; -- Part 2


(deftest part2_test
  (is (part2) 5003530))
