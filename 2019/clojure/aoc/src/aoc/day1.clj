(ns aoc-day1
  (:require [clojure.string :refer [split-lines]]
            [clojure.test :refer [deftest is are]]))

(def inputs 
 (map #(Integer. %) 
  (map read-string (split-lines (slurp "../inputs/day1_input.txt")))))

(defn fuel [mass]
  (- (quot mass 3) 2))

(defn part1 []
  (reduce + (map fuel inputs)))

(deftest part1_test
  (is (part1) 3337604))


;; -- Part 2

(defn total-fuel [mass]
  (reduce + (take-while pos? (drop 1 (iterate fuel mass)))))

(defn part2 []
  (reduce + (map total-fuel inputs)))

(deftest part2_test
  (is (part2) 5003530))
