(ns advent-2019.day01
  (:require [clojure.string :refer [split-lines]]))

(defn parse-int
  [number-string]
  "parses string to integer"
  (Integer/parseInt number-string 10))

(def input
  (map parse-int (split-lines (slurp "/home/sergiusz/workspace/adventofcode/2019/input/day1_input.txt"))))

(defn fuel
  [mass]
  "Determines how much fuel is needed to launch a given mass"
  (- (quot mass 3) 2))

(def part1
  (reduce + (map fuel input)))

(defn total-fuel
  [mass]
  "Determines how much fuel is needed to launch a given mass plus its fuel"
  (reduce + (take-while pos? (rest (iterate fuel mass)))))

(def part2
  (reduce + (map total-fuel input)))

(def othermain
  (println [part1, part2]))

(defn sample
  [num]
  (take num input))

(iterate fuel 12121)

(println (take 5 input))
